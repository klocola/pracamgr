﻿using System.Linq;
using EvolutionFramework;

namespace DeceptiveOneMaxExample
{
    public class Fitness : AbstractFitness<int[]>
    {
        public override double CalculateFitness(Population<int[]> population, Individual<int[]> individual)
        {
            int sum = individual.Chromosome.Sum();
            return sum == 0 ? individual.Chromosome.Length + 1 : sum;
        }
    }
}
