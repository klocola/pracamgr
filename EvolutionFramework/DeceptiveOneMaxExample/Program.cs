﻿using EvolutionFramework;
using System;

namespace DeceptiveOneMaxExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine<int[]> engine = new Engine<int[]>(new Configuration());
            engine.RunEvolution();
            Console.ReadLine();
        }
    }
}
