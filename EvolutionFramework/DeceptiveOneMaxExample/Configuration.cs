﻿using EvolutionFramework;

namespace DeceptiveOneMaxExample
{
    public class Configuration : AbstractBinaryArrayConfiguration
    {
        public override IFitness<int[]> Fitness
        {
            get
            {
                return new Fitness();
            }
        }
    }
}
