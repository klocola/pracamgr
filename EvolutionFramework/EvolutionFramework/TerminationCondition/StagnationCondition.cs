﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Extensions;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements condition of ending evaluation according to existence of stagnation.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class StagnationCondition<T> : ITerminationCondition<T>
    {
        private double _prevBestFitness;
        private int _stagnationGenerationCount = 1;
        private readonly int _maxStagnationGenerationCount;

        /// <summary>
        /// Constructor of stagnation condition.
        /// </summary>
        /// <param name="maxStagnationGenerationCount">Limit number of generations in which max fitness is constant.</param>
        public StagnationCondition(int maxStagnationGenerationCount)
        {
            _maxStagnationGenerationCount = maxStagnationGenerationCount;
        }

        /// <summary>
        /// Determine whether evolution should be end according to existence of stagnation.
        /// </summary>
        /// <param name="population">Population which is evolved.</param>
        /// <returns>Returns true if generation number with stagnation is greater than max stagnation generation count otherwise returns false.</returns>
        public bool IsEvolutionOver(Population<T> population)
        {
            if (_prevBestFitness.EqualsWeakPrecision(population.BestParentFitness) ||
                _prevBestFitness > population.BestParentFitness)
            {
                if (_maxStagnationGenerationCount < _stagnationGenerationCount)
                {
                    return true;
                }

                _stagnationGenerationCount++;
                return false;
            }

            _stagnationGenerationCount = 1;
            _prevBestFitness = population.BestParentFitness;
            return false;
        }
    }
}
