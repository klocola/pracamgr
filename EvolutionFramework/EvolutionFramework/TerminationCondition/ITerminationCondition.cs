﻿namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements condition of ending evaluation.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public interface ITerminationCondition<T>
    {
        /// <summary>
        /// Determine whether evolution should be end.
        /// </summary>
        /// <param name="population">Population which is evolved.</param>
        /// <returns>Returns true if evolution should be end otherwise returns false.</returns>
        bool IsEvolutionOver(Population<T> population);
    }
}
