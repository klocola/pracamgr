﻿namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements condition of ending evaluation according to value of the greatest fitness.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class TargetBestFitnessCondition<T> : ITerminationCondition<T>
    {
        private readonly double _targetFitness;

        /// <summary>
        /// Constructor of target best fitness condition.
        /// </summary>
        /// <param name="targetFitness">Target value of fitness.</param>
        public TargetBestFitnessCondition(double targetFitness)
        {
            _targetFitness = targetFitness;
        }

        /// <summary>
        /// Determine whether evolution should be end according to achievement target value of the greatest fitness.
        /// </summary>
        /// <param name="population">Population which is evolved.</param>
        /// <returns>Returns true if best value of fitness is greater than target value of greatest fitness otherwise returns false.</returns>
        public bool IsEvolutionOver(Population<T> population)
        {
            return population.BestParentFitness >= _targetFitness;
        }
    }
}
