﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements condition of ending evaluation according to duration of evaluation.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class TimeLimitationCondition<T> : ITerminationCondition<T>
    {
        private readonly TimeSpan _maxDuration;

        /// <summary>
        /// Constructor of time limitation condition.
        /// </summary>
        /// <param name="maxDuration">Max duration of evaluation.</param>
        public TimeLimitationCondition(TimeSpan maxDuration)
        {
            _maxDuration = maxDuration;
        }

        /// <summary>
        /// Determine whether evolution should be end according to duration of evaluation.
        /// </summary>
        /// <param name="population">Population which is evolved.</param>
        /// <returns>Returns true if duration of evaluation is greater max duration of evaluation count otherwise returns false.</returns>
        public bool IsEvolutionOver(Population<T> population)
        {
            return _maxDuration <= (DateTime.UtcNow - population.StartTime);
        }
    }
}
