﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements condition of ending evaluation according to achievement max number of generations.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class GenerationCountLimitationCondition<T> : ITerminationCondition<T>
    {
        private readonly int _maxGenerationsCount;
        
        /// <summary>
        /// Constructor of generation count limitation.
        /// </summary>
        /// <param name="maxGenerationsCount">Limit number of generations.</param>
        public GenerationCountLimitationCondition(int maxGenerationsCount)
        {
            _maxGenerationsCount = maxGenerationsCount;
        }

        /// <summary>
        /// Determine whether evolution should be end according to total count of generation.
        /// </summary>
        /// <param name="population">Population which is evolved.</param>
        /// <returns>Returns true if actual generation number is greater than max generation count otherwise returns false.</returns>
        public bool IsEvolutionOver(Population<T> population)
        {
            return _maxGenerationsCount <= population.GenerationNumber;
        }
    }
}
