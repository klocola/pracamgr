﻿using System;

namespace EvolutionFramework
{
    public interface IRandom
    {
        /// <summary>
        /// Returns a random number within a specified range.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
    	Int32 Next(Int32 min, Int32 max);	

        /// <summary>
        /// Returns a random number between 0.0 and 1.0.
        /// </summary>
        /// <returns></returns>
        double NextDouble();
    }
}
