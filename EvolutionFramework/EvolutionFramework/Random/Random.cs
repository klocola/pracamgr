﻿namespace EvolutionFramework
{
    public class Random : IRandom
    {
        readonly System.Random _systemRandom;

        public Random()
        {
            _systemRandom = new System.Random();
        }

        public Random(int seed)
        {
            _systemRandom = new System.Random(seed);
        }

         public int Next(int min, int max)
        {
            return _systemRandom.Next(min, max);
        }

        public double NextDouble()
        {
            return _systemRandom.NextDouble();
        }
    }
}
