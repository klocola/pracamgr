using System;

namespace EvolutionFramework
{
    /// <summary>
    /// Exception for incorrect configuration.
    /// </summary>
    public class IncorrectConfigurationException : Exception
    {
        public IncorrectConfigurationException(string message)
            :base(message)
        {
        }
    }
}