﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utils.Extensions;

namespace EvolutionFramework
{
    /// <summary>
    /// Main class in library, which coordinates the process of evolution.
    /// </summary>
    /// <typeparam name="T">Type of the chromosome.</typeparam>
    public class Engine<T>
    {
        private Population<T> _population;

        /// <summary>
        /// Fitness function. 
        /// </summary>
        public IFitness<T> Fitness { get; private set; }

        /// <summary>
        /// Printer to print the most importatnt information in every generation.
        /// </summary>
        public AbstractPrinter<T> Printer { get; private set; }

        /// <summary>
        /// Condition which determine whether evolution should stop.
        /// </summary>
        public ITerminationCondition<T> TerminateCondition { get; private set; }

        /// <summary>
        /// Defines whether fitness may change even if chromosome did not,
        /// eg. when fitness is computed based on whole population.
        /// </summary>
        public bool IsFitnessVariable { get; private set; }

        /// <summary>
        /// Count of individuals in population. 
        /// </summary>
        public int PopulationSize { get; private set; }

        /// <summary>
        /// Factory which produes population.
        /// </summary>
        public IIndividualsFactory<T> IndividualsFactory { get; private set; }

        /// <summary>
        /// Collection of evolution tools, such as: crossover, mutation and selection.
        /// Order of collection derermines the order of executing operations.
        /// </summary>
        public IEnumerable<IEvolutionTool<T>> EvolutionTools { get; private set; }
        
        private Population<T> Population
        {
            get
            {
                if (_population == null)
                {
                    _population = new Population<T>(IndividualsFactory.GenerateIndividuals(PopulationSize));
                }

                return _population;
            }
        }

        /// <summary>
        /// Constructor of engine.
        /// </summary>
        /// <param name="configuration">Concrete configuration.</param>
        public Engine(IConfiguration<T> configuration)
        {
            Fitness = configuration.Fitness;
            Printer = configuration.Printer;
            TerminateCondition = configuration.TerminateCondition;
            IsFitnessVariable = configuration.IsFitnessVariable;
            PopulationSize = configuration.PopulationSize;
            EvolutionTools = configuration.EvolutionTools;
            IndividualsFactory = configuration.IndividualsFactory;
        }

        private void CheckConfiguration()
        {
            if (Fitness == null)
            {
                throw new IncorrectConfigurationException("Fitness is not definied.");
            }

            if (TerminateCondition == null)
            {
                throw new IncorrectConfigurationException("Terminate condition is not definied.");
            }

            if (EvolutionTools == null)
            {
                throw new IncorrectConfigurationException("Evolution tools are not definied.");
            }

            if (PopulationSize == 0)
            {
                throw new IncorrectConfigurationException("Population size must be greater than 0.");
            }

            if (IndividualsFactory == null)
            {
                throw new IncorrectConfigurationException("Individual factory is not definied.");
            }

            if (EvolutionTools.Count() == 0)
            {
                throw new IncorrectConfigurationException("There are no evolution tools defined");
            }

            if (PopulationSize <= 0)
            {
                throw new IncorrectConfigurationException("Population size must be grater than zero.");
            }
        }

        private void UpdateFitness()
        {
            Fitness.UpdateChildrenFitness(Population);

            if (IsFitnessVariable)
            {
                Fitness.UpdateParentsFitness(Population);
            }
        }

        /// <summary>
        /// Runs evolution.
        /// </summary>
        public void RunEvolution()
        {
            CheckConfiguration();
            Fitness.UpdateParentsFitness(Population);

            double prevMaxFitness = Double.MinValue;
            while (!TerminateCondition.IsEvolutionOver(Population))
            {
                foreach (IEvolutionTool<T> evolutionTool in EvolutionTools)
                {
                    evolutionTool.Apply(Population);
                    UpdateFitness();
                }

                Population.ClearChildren();
                if (!prevMaxFitness.EqualsWeakPrecision(Population.BestParentFitness))
                {
                    Printer.Print(Population, Population.GenerationNumber);
                    prevMaxFitness = Population.BestParentFitness;
                }

                Population.GenerationNumber++;
            }
        }
    }
}