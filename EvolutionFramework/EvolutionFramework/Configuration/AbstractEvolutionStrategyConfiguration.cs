﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;
using Utils;

namespace EvolutionFramework
{
    public abstract class AbstractEvolutionStrategyConfiguration : IConfiguration<double[]>
    {
        private const int DefaultPopulationSize = 300;
        private const int DefaultChildreCount = 300;
        private const int DefaultMaxStagnationGenerationCount = 600;

        private const double DefaultMutationRange = 0.7;
        private const double DefaultMutationChangeBitProbability = 0.7;

        protected readonly IRandom Random = new EvolutionFramework.Random();
        /// <summary>
        /// Fitness function. 
        /// </summary>
        public abstract IFitness<double[]> Fitness { get; }

        /// <summary>
        /// Factory which produce population.
        /// </summary>
        public abstract IIndividualsFactory<double[]> IndividualsFactory { get; }

        /// <summary>
        /// Defines whether fitness may change even if chromosome did not,
        /// eg. when fitness is computed based on whole population.
        /// </summary>
        public bool IsFitnessVariable
        {
            get { return false; }
        }

        /// <summary>
        /// Count of individuals in population. 
        /// </summary>
        public int PopulationSize
        {
            get { return DefaultPopulationSize; }
        }

        /// <summary>
        /// Printer to print the most importatnt information in every generation.
        /// </summary>
        public AbstractPrinter<double[]> Printer
        {
            get { return new ArrayPrinter<double>(); }
        }

        /// <summary>
        /// Condition which determine whether evolution should stop.
        /// </summary>
        public ITerminationCondition<double[]> TerminateCondition
        {
            get {return new StagnationCondition<double[]>(DefaultMaxStagnationGenerationCount); }
        }

        /// <summary>
        /// Collection of evoltion tools, such as: crossover, mutation and selection.
        /// Order of collection derermine order of executing operations).
        /// </summary>
        public IEnumerable<IEvolutionTool<double[]>> EvolutionTools
        {
            get
            {
                return new List<IEvolutionTool<double[]>> 
                { 
                    new EvolutionStrategyCombinedOperatorsWithAutoAdaptation<double[]>( Random,
                        (AbstractFitness<double[]>)Fitness,
                        DefaultChildreCount,
                        new RangeMutationLogic(Random, DefaultMutationChangeBitProbability, DefaultMutationRange),
                        new TwoPointCrossOverLogic<double>(Random)),
                    new FromParentsAndChildrenEliteSelection<double[]>(DefaultPopulationSize)
                };
            }
        }
    }
}
