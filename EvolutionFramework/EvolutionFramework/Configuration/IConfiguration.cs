﻿using System.Collections.Generic;

namespace EvolutionFramework
{
    /// <summary>
    /// Configuration interface.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public interface IConfiguration<T>
    {
        /// <summary>
        /// Defines whether fitness may change even if chromosome did not,
        /// eg. when fitness is computed based on whole population.
        /// </summary>
        bool IsFitnessVariable { get; }
        
        /// <summary>
        /// Count of individuals in population. 
        /// </summary>
        int PopulationSize { get; }
        
        /// <summary>
        /// Fitness function. 
        /// </summary>
        IFitness<T> Fitness { get; }

        /// <summary>
        /// Printer to print the most importatnt information in every generation.
        /// </summary>
        AbstractPrinter<T> Printer { get; }

        /// <summary>
        /// Condition which determine whether evolution should stop.
        /// </summary>
        ITerminationCondition<T> TerminateCondition { get; }

        /// <summary>
        /// Collection of evoltion tools, such as: crossover, mutation and selection.
        /// Order of collection derermine order of executing operations).
        /// </summary>
        IEnumerable<IEvolutionTool<T>> EvolutionTools { get;}

        /// <summary>
        /// Factory which produce population.
        /// </summary>
        IIndividualsFactory<T> IndividualsFactory { get; }
    }
}
