﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;
using Utils;

namespace TSPExample
{
    /// <summary>
    /// Abstract configuration for problems with chromosomes which are permutation of some elements.
    /// </summary>
    public abstract class AbstractPermutationConfiguration<T> : IConfiguration<T[]> where T : IComparable
    {
        private const int DefaultPopulationSize = 300;
        private const int DefaultMaxStagnationGenerationCount = 50;

        private const double DefaultInversionMutationProbability = 0.05;
        private const double DefaultPartiallyMatchedCrossOverProbability = 0.6;

        private readonly IRandom _random = new EvolutionFramework.Random();
        private readonly T[] _elementsInPermutation;

        /// <summary>
        /// Fitness function. 
        /// </summary>
        public abstract IFitness<T[]> Fitness { get; }

        /// <summary>
        /// Defines whether fitness may change even if chromosome did not,
        /// eg. when fitness is computed based on whole population.
        /// </summary>
        public virtual bool IsFitnessVariable
        {
            get { return false; }
        }

        /// <summary>
        /// Count of individuals in population. 
        /// </summary>
        public virtual int PopulationSize
        {
            get { return DefaultPopulationSize; }
        }

        /// <summary>
        /// Printer to print the most importatnt information in every generation.
        /// </summary>
        public virtual AbstractPrinter<T[]> Printer
        {
            get { return new ArrayPrinter<T>(); }
        }

        /// <summary>
        /// Condition which determine whether evolution should stop.
        /// </summary>
        public virtual ITerminationCondition<T[]> TerminateCondition
        {
            get
            {
                return new StagnationCondition<T[]>(DefaultMaxStagnationGenerationCount);
            }
        }

        /// <summary>
        /// Collection of evoltion tools, such as: crossover, mutation and selection.
        /// Order of collection derermine order of executing operations).
        /// </summary>
        public virtual IEnumerable<IEvolutionTool<T[]>> EvolutionTools
        {
            get
            {
                return new List<IEvolutionTool<T[]>> 
                { 
                    new InversionMutation<T>(_random, DefaultInversionMutationProbability),
                    new PartiallyMatchedCrossOver<T>(_random, DefaultPartiallyMatchedCrossOverProbability),
                    new FromParentsAndChildrenRouletteWheelSelection<T[]>(_random, PopulationSize)
                };
            }
        }

        /// <summary>
        /// Factory which produce population.
        /// </summary>
        public virtual IIndividualsFactory<T[]> IndividualsFactory
        {
            get
            {
                return new PermutationIndividualFactory<T>(_random, _elementsInPermutation);
            }
        }

        /// <summary>
        /// Abstract constructor.
        /// </summary>
        /// <param name="elementsInPermutation">Collection of elements in permutation.</param>
        protected AbstractPermutationConfiguration(T[] elementsInPermutation)
        {
            _elementsInPermutation = elementsInPermutation;
        }
    }
}
