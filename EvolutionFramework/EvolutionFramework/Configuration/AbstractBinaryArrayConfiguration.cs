﻿using System;
using System.Collections.Generic;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Abstract configuration for problems with chromosomes which are arrays of bits.
    /// </summary>
    public abstract class AbstractBinaryArrayConfiguration : IConfiguration<int[]>
    {
        private const int DefaultChromosomeSize = 60;
        private const int DefaultPopulationSize = 300;
        private const int DefaultMaxStagnationGenerationCount = 50;

        private const double DefaultBinaryChangeMutationProbability = 0.2;
        private const double DefaultBinaryChangeMutationChangeBitProbability = 0.3;
        private const double DefaultOnePointCrossOverProbability = 0.7;
        private const double DefaultBinaryRandomMutatioMutationProbability = 0.3;
        private const double DefaultBinaryRandomMutationChangeBitProbability = 0.2;

        private readonly IRandom _random = new EvolutionFramework.Random();

        /// <summary>
        /// Fitness function. 
        /// </summary>
        public abstract IFitness<int[]> Fitness { get; }

        /// <summary>
        /// Specified chromosome size.
        /// </summary>
        protected virtual int ChromosomeSize
        {
            get { return DefaultChromosomeSize; }
        }

        /// <summary>
        /// Specified target fitness.
        /// </summary>
        protected virtual int TargetFitness
        {
            get { return ChromosomeSize + 1; }
        }

        /// <summary>
        /// Defines whether fitness may change even if chromosome did not,
        /// eg. when fitness is computed based on whole population.
        /// </summary>
        public virtual bool IsFitnessVariable
        {
            get { return false; }
        }

        /// <summary>
        /// Count of individuals in population. 
        /// </summary>
        public virtual int PopulationSize
        {
            get { return DefaultPopulationSize; }
        }

        /// <summary>
        /// Printer to print the most importatnt information in every generation.
        /// </summary>
        public virtual AbstractPrinter<int[]> Printer
        {
            get { return new ArrayPrinter<int>(); }
        }

        /// <summary>
        /// Condition which determine whether evolution should stop.
        /// </summary>
        public virtual ITerminationCondition<int[]> TerminateCondition
        {
            get
            {
                return new StagnationCondition<int[]>(DefaultMaxStagnationGenerationCount);
            }
        }

        /// <summary>
        /// Collection of evoltion tools, such as: crossover, mutation and selection.
        /// Order of collection derermine order of executing operations).
        /// </summary>
        public virtual IEnumerable<IEvolutionTool<int[]>> EvolutionTools
        {
            get
            {
                return new List<IEvolutionTool<int[]>> 
                { 
                    new BinaryChangeMutation(_random, DefaultBinaryChangeMutationProbability, DefaultBinaryChangeMutationChangeBitProbability),
                    new OnePointCrossOver<int>(_random, DefaultOnePointCrossOverProbability),
                    new BinaryRandomMutation(_random, DefaultBinaryRandomMutatioMutationProbability, DefaultBinaryRandomMutationChangeBitProbability),
                    new FromParentsAndChildrenEliteSelection<int[]>(PopulationSize)
                };
            }
        }

        /// <summary>
        /// Factory which produce population.
        /// </summary>
        public virtual IIndividualsFactory<int[]> IndividualsFactory
        {
            get
            {
                return new BinaryArrayIndividualFactory(_random, ChromosomeSize);
            }
        }
    }
}
