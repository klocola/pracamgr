﻿namespace EvolutionFramework
{
    /// <summary>
    /// Interface for fitness function.
    /// </summary>
    /// <typeparam name="T">Type ogfchromosome.</typeparam>
    public interface IFitness<T>
    {
        /// <summary>
        /// Updates parents fitness in population.
        /// </summary>
        /// <param name="population">Population in which fitnessed will be updating.</param>
        void UpdateParentsFitness(Population<T> population);

        /// <summary>
        /// Updates children fitness in population.
        /// </summary>
        /// <param name="population">Population in which fitnessed will be updating.</param>
        void UpdateChildrenFitness(Population<T> population);
    }
}
