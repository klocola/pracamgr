﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvolutionFramework
{
    /// <summary>
    /// Abstract class which implements fitness function.
    /// </summary>
    /// <typeparam name="T">Type og chromosome.</typeparam>
    public abstract class AbstractFitness<T> : IFitness<T>
    {
        /// <summary>
        /// Calcutates selected individual fitness.
        /// </summary>
        /// <param name="population">Poplation in which selected individual exists.</param>
        /// <param name="individual">Individual which fitness will be calculating.</param>
        /// <returns>Calculated fitness.</returns>
        public abstract double CalculateFitness(Population<T> population, Individual<T> individual);

        /// <summary>
        /// Updates individuals fitness in population.
        /// </summary>
        /// <param name="population">Population in which fitnessed will be updating.</param>
        public void UpdateParentsFitness(Population<T> population)
        {
            foreach (Individual<T> individual in population.Parents)
            {
                double fitness = CalculateFitness(population, individual);
                individual.Fitness = fitness;
            }
        }

        /// <summary>
        /// Updates children fitness in population.
        /// </summary>
        /// <param name="population">Population in which fitnessed will be updating.</param>
        public void UpdateChildrenFitness(Population<T> population)
        {
            foreach (Individual<T> child in population.Children)
            {
                double fitness = CalculateFitness(population, child);
                child.Fitness = fitness;
            }
        }
    }
}
