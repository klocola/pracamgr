﻿namespace EvolutionFramework
{
    /// <summary>
    /// Abstract class which implements fitness decorator.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public abstract class AbstractFitnessDecorator<T> : IFitness<T>
    {
        private readonly IFitness<T> _fitness;

        /// <summary>
        /// Constructor of fitness decorator.
        /// </summary>
        /// <param name="fitness">Fitness class which will calculate fitness of each individuals.</param>
        protected AbstractFitnessDecorator(IFitness<T> fitness)
        {
            _fitness = fitness;
        }

        /// <summary>
        /// Abstract method which adapts fitness,
        /// eg. convert each fitness from signed value to unsigned.
        /// </summary>
        /// <param name="population">Population which individual's fitnesses will be adapting.</param>
        protected abstract void AdaptFitness(Population<T> population);

        /// <summary>
        /// Updates individuals fitness in population.
        /// </summary>
        /// <param name="population">Population in which fitnessed will be updating.</param>
        public void UpdateParentsFitness(Population<T> population)
        {
            _fitness.UpdateParentsFitness(population);
            AdaptFitness(population);
        }

        /// <summary>
        /// Updates children fitness in population.
        /// </summary>
        /// <param name="population">Population in which fitnessed will be updating.</param>
        public void UpdateChildrenFitness(Population<T> population)
        {
            _fitness.UpdateChildrenFitness(population);
            AdaptFitness(population);
        }
    }
}
