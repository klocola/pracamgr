﻿using System;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements individual.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class Individual<T>
    {
        /// <summary>
        /// Chromosome which represents individual.
        /// </summary>
        public T Chromosome { get; private set; }

        /// <summary>
        /// Calculated fitness of individual.
        /// </summary>
        public double? Fitness { get; set; }

        /// <summary>
        /// Constructor of individual.
        /// </summary>
        /// <param name="chromosome">Chromosome which represents individual.</param>
        public Individual(T chromosome)
        {
            Chromosome = chromosome;
        }
    }
}
