﻿using System;

namespace EvolutionFramework
{
    public class ArrayPrinter<T> : AbstractPrinter<T[]>
    {
        public override string GetMessage(Population<T[]> population, int generationNumber)
        {
            Individual<T[]> bestParent = population.BestParent;
            string result = generationNumber + ": ";
            foreach (T gene in bestParent.Chromosome)
            {
                result += gene + " ";
            }

            result += " - " + bestParent.Fitness;
            return result;
        }
    }
}
