﻿using System;

namespace EvolutionFramework
{
    abstract public class AbstractPrinter<T>
    {
        public void Print(Population<T> population, int generationNumber)
        {
            Console.WriteLine(GetMessage(population, generationNumber));
        }

        public abstract string GetMessage(Population<T> population, int generationNumber);
    }
}
