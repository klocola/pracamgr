﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which generates individuals (unsigned double arraies)
    /// </summary>
    public class UnsignedDoubleArrayIndividualFactory : DoubleArrayIndividualFactory
    {
        /// <summary>
        /// Constructor of unsigned double individual factory.
        /// </summary>
        /// <param name="random">Count of generated individuals.</param>
        /// <param name="chromosomeSize">Size of chromosome.</param>
        public UnsignedDoubleArrayIndividualFactory(IRandom random, int chromosomeSize)
            : base(random, chromosomeSize)
        {
        }

        /// <summary>
        /// Constructor of unsigned double individual factory.
        /// </summary>
        /// <param name="random">Count of generated individuals.</param>
        /// <param name="range">Range of</param>
        /// <param name="chromosomeSize">Size of chromosome.</param>
        public UnsignedDoubleArrayIndividualFactory(IRandom random, int chromosomeSize, double range)
            : base(random, chromosomeSize, range)
        {
        }

        protected override double Scale(double value)
        {
            return value * Range;
        }
    }
}

