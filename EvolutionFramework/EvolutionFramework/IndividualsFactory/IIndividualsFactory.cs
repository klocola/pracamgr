﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvolutionFramework
{
    public interface IIndividualsFactory<T>
    {
        /// <summary>
        /// Generates single individual.
        /// </summary>
        /// <returns>Generated individual.</returns>
        Individual<T> GenerateIndividual();

        /// <summary>
        /// Generates specified count of individuals.
        /// </summary>
        /// <param name="individualsCout">Count of generated individuals.</param>
        /// <returns>Generated individuals.</returns>
        IEnumerable<Individual<T>> GenerateIndividuals(int individualsCout);
    }
}
