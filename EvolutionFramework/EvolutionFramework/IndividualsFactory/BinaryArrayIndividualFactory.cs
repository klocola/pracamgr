﻿using System;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which generates individuals (binary arraies)
    /// </summary>
    public class BinaryArrayIndividualFactory : AbstractRandomIndividualsFactory<int[]>
    {
        private readonly int _chromosomeSize;

        /// <summary>
        /// Constructor of binary individual factory.
        /// </summary>
        /// <param name="random">Count of generated individuals.</param>
        /// <param name="chromosomeSize">Size of chromosome.</param>
        public BinaryArrayIndividualFactory(IRandom random, int chromosomeSize)
            :base(random)
        {
            _chromosomeSize = chromosomeSize;
        }

        /// <summary>
        /// Generates single individual.
        /// </summary>
        /// <returns>Generated individual.</returns>
        public override Individual<int[]> GenerateIndividual()
        {
            int[] newChromosome = new int[_chromosomeSize];
            for(int i = 0; i < _chromosomeSize; i++)
            {
                newChromosome[i] = (int)Math.Round(Random.NextDouble());
            }
            
            return  new Individual<int[]>(newChromosome);
        }
    }
}
