﻿using System.Collections.Generic;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Abstract class which implements factory of individuals with random element.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public abstract class AbstractRandomIndividualsFactory<T> : IIndividualsFactory<T>
    {
        /// <summary>
        /// Class for generating random number.
        /// </summary>
        protected IRandom Random { get; set; }

        /// <summary>
        /// Constructor of abstract random individual factory.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        protected AbstractRandomIndividualsFactory(IRandom random)
        {
            Random = random;
        }

        /// <summary>
        /// Generates single individual.
        /// </summary>
        /// <returns>Generated individual.</returns>
        public abstract Individual<T> GenerateIndividual();
        
        /// <summary>
        /// Generates specified count of individuals.
        /// </summary>
        /// <param name="individualsCout">Count of generated individuals.</param>
        /// <returns>Generated individuals.</returns>
        public virtual IEnumerable<Individual<T>> GenerateIndividuals(int individualsCout)
        {
            for (int i = 0; i < individualsCout; i++)
            {
                yield return GenerateIndividual();
            }
        }
    }
}
