﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which generates individuals (double arraies)
    /// </summary>
    public class DoubleArrayIndividualFactory : AbstractRandomIndividualsFactory<double[]>
    {
        private readonly int _chromosomeSize;
        protected readonly double Range = 1;

        /// <summary>
        /// Constructor of double individual factory.
        /// </summary>
        /// <param name="random">Count of generated individuals.</param>
        /// <param name="chromosomeSize">Size of chromosome.</param>
        public DoubleArrayIndividualFactory(IRandom random, int chromosomeSize) 
            : base(random)
        {
            _chromosomeSize = chromosomeSize;
        }

        /// <summary>
        /// Constructor of double individual factory.
        /// </summary>
        /// <param name="random">Count of generated individuals.</param>
        /// <param name="range">Range of</param>
        /// <param name="chromosomeSize">Size of chromosome.</param>
        public DoubleArrayIndividualFactory(IRandom random, int chromosomeSize, double range)
            : base(random)
        {
            _chromosomeSize = chromosomeSize;
            Range = range;
        }

        /// <summary>
        /// Generates single individual.
        /// </summary>
        /// <returns>Generated individual.</returns>
        public override Individual<double[]> GenerateIndividual()
        {
            double[] newChromosome = new double[_chromosomeSize];
            for(int i = 0; i < _chromosomeSize; i++)
            {
                newChromosome[i] = Scale(Random.NextDouble());
            }
            
            return  new Individual<double[]>(newChromosome);
        }

        protected virtual double Scale(double value)
        {
            return (value - 0.5) * 2 * Range;
        }
    }
}
