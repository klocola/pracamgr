﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Extensions;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which generates individuals (permutation of genes)
    /// </summary>
    public class PermutationIndividualFactory<T> : AbstractRandomIndividualsFactory<T[]>
    {
        private readonly T[] _genes;

        /// <summary>
        /// Constructor of permuitation individual factory.
        /// </summary>
        /// <param name="random">Count of generated individuals.</param>
        /// <param name="genes">Genes which chromosome contains.</param>
        public PermutationIndividualFactory(IRandom random, T[] genes)
            : base(random)
        {
            _genes = genes;
        }

        /// <summary>
        /// Generates single individual.
        /// </summary>
        /// <returns>Generated individual.</returns>
        public override Individual<T[]> GenerateIndividual()
        {
            T[] newChromosome = MixList(_genes, Random).ToArray();
            return new Individual<T[]>(newChromosome);
        }

        public static IEnumerable<T> MixList(IList<T> list, IRandom random)
        {
            int[] indexes = Enumerable.Range(0, list.Count).ToArray();
            for (int i = 0; i < list.Count; ++i)
            {
                int position = random.Next(i, list.Count);

                yield return list[indexes[position]];

                indexes[position] = indexes[i];
            }
        }
    }
}
