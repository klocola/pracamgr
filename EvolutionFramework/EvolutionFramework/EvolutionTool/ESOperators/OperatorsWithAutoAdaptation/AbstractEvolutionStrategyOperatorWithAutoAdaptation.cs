﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements operator for evolution strategy with auto adaptation (Rechenberg's 1/5 success rule).
    /// </summary>
    abstract public class AbstractEvolutionStrategyOperatorWithAutoAdaptation<T> : IEvolutionTool<T>
    {
        private readonly AbstractFitness<T> _fitness;
        protected readonly IMutationLogicWithRange<T> MutationLogicWithRange;
        private int _successCount = 0;
        private int _totalMutationCount = 0;
        private int _iterationCount = 0;
        private int _updateIterationNumber = 100;
        private double _decreasingRangeScale = 0.92;
        private double _increasingRangeScale = 1.22;

        /// <summary>
        /// Number of iteration after which the mutation range will be updated.
        /// </summary>
        public int UpdateIterationNumber
        {
            get { return _updateIterationNumber; }
            set { _updateIterationNumber = value; }
        }

        /// <summary>
        /// Scale for decreasing range od mutation (smaller than 1)
        /// </summary>
        public double DecreasingRangeScale 
        { 
            get { return _decreasingRangeScale; }
            set
            {
                _decreasingRangeScale = value;
                if (_decreasingRangeScale >= 1)
                {
                    _decreasingRangeScale = 0.99;
                }
            }
        }

        /// <summary>
        /// Scale for increasing range od mutation (bigger than 1)
        /// </summary>
        public double IncreasigRangeScale 
        {
            get { return _increasingRangeScale; }
            set {
                _increasingRangeScale = value;
                if (_increasingRangeScale <= 1)
                {
                    _increasingRangeScale = 1.01;
                }
            }
        }

        /// <summary>
        /// Constructor of evolution strategy operators with auto adaptation.
        /// </summary>
        /// <param name="fitness">Fitness function for calculating child's fitness.</param>
        /// <param name="mutationLogicWithRange">Logic of mutation with range (for auto adaptation)</param>
        protected AbstractEvolutionStrategyOperatorWithAutoAdaptation(AbstractFitness<T> fitness, IMutationLogicWithRange<T> mutationLogicWithRange)
        {
            _fitness = fitness;
            MutationLogicWithRange = mutationLogicWithRange;
        }

        /// <summary>
        /// Applies operator to whole population and adapts range of mutation.
        /// </summary>
        /// <param name="population">Population on which operators will operate.</param>
        public abstract void Apply(Population<T> population);

        /// <summary>
        /// Increases iteration count and try to adapt range of mutation.
        /// </summary>
        protected virtual void TryAdaptMutationRange()
        {
            _iterationCount++;
            if (_iterationCount == UpdateIterationNumber)
            {
                if (_successCount > _totalMutationCount * (1.0/5.0))
                {
                    MutationLogicWithRange.Range = MutationLogicWithRange.Range * IncreasigRangeScale;
                }
                else
                {
                    MutationLogicWithRange.Range = MutationLogicWithRange.Range * DecreasingRangeScale;
                }

                _iterationCount = 0;
                _successCount = 0;
                _totalMutationCount = 0;
            }
        }

        /// <summary>
        /// Increaces total count of mutation and increases success count if the child is better than its parent.
        /// </summary>
        /// <param name="population">Population of child and parent.</param>
        /// <param name="child">Produced child.</param>
        /// <param name="parent">Paretn od produced child.</param>
        protected virtual void UpdateStatistic(Population<T> population, Individual<T> child, Individual<T> parent)
        {
            child.Fitness = _fitness.CalculateFitness(population, child);
            if (child.Fitness > parent.Fitness)
            {
                _successCount++;
            }

            _totalMutationCount++;
        }
    }
}

