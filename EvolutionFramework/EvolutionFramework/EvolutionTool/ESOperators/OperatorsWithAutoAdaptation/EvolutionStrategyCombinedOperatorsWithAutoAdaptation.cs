﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements combined operator for evolution strategy with auto adaptation of mutation(Rechenberg's 1/5 success rule).
    /// </summary>
    public class EvolutionStrategyCombinedOperatorsWithAutoAdaptation<T> : AbstractEvolutionStrategyOperatorWithAutoAdaptation<T>
    {
        private readonly IRandom _random;
        private readonly int _childrenCount;
        private readonly ICrossOverLogic<T> _crossOverLogic; 

        /// <summary>
        /// Constructor of combined operators for evolution strategy with auto adaptation.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        /// <param name="fitness">Fitness function for calculating child's fitness.</param>
        /// <param name="childrenCount">Count of children to producee.</param>
        /// <param name="mutationLogicWithRange">Logic of mutation with range (for auto adaptation)</param>
        /// <param name="crossOverLogic">Logic of crossover.</param>
        public EvolutionStrategyCombinedOperatorsWithAutoAdaptation(IRandom random, AbstractFitness<T> fitness, int childrenCount, IMutationLogicWithRange<T> mutationLogicWithRange, ICrossOverLogic<T> crossOverLogic)
            : base(fitness, mutationLogicWithRange)
        {
            _random = random;
            _childrenCount = childrenCount;
            _crossOverLogic = crossOverLogic;
        }

        /// <summary>
        /// Applies mutation to whole population.
        /// </summary>
        /// <param name="population">Population on which mutation will operate.</param>
        public override void Apply(Population<T> population)
        {
            List<Individual<T>> children = new List<Individual<T>>();

            //CrossOver
            while (children.Count < _childrenCount)
            {
                List<Individual<T>> selectedParent = new List<Individual<T>>();
                for (int index = 0; index < _crossOverLogic.ParentCount; index++)
                {
                    selectedParent.Add(population.Parents.ElementAt(_random.Next(0, population.Parents.Count)));
                }

                children.AddRange(_crossOverLogic
                    .CrossOver(selectedParent.Select(parent => parent.Chromosome).ToList())
                    .Select(child => new Individual<T>(child)));
            }

            children.RemoveRange(_childrenCount, children.Count - _childrenCount);

            //mutate
            TryAdaptMutationRange();
            foreach (Individual<T> child in children)
            {
                Individual<T> newIndividual =
                    new Individual<T>(MutationLogicWithRange.Mutate(child.Chromosome));
                UpdateStatistic(population, newIndividual, child);
                population.Children.Add(newIndividual);
            }
        }
    }
}

