﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements mutation for evolution strategy with auto adaptation of mutation(Rechenberg's 1/5 success rule)..
    /// </summary>
    public class EvolutionStrategyMutationWithAutoAdaptation<T> : AbstractEvolutionStrategyOperatorWithAutoAdaptation<T>
    {
        private readonly IRandom _random;
        private readonly int _childrenCount;

        /// <summary>
        /// Constructor of mutation for evolution strategy with auto adaptation.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        /// <param name="fitness">Fitness function for calculating child's fitness.</param>
        /// <param name="childrenCount">Count of children to producee.</param>
        /// <param name="mutationLogicWithRange">Logic of mutation with range (for auto adaptation)</param>
        public EvolutionStrategyMutationWithAutoAdaptation(IRandom random, AbstractFitness<T> fitness, int childrenCount, IMutationLogicWithRange<T> mutationLogicWithRange )
            : base(fitness, mutationLogicWithRange)
        {
            _random = random;
            _childrenCount = childrenCount;
        }

        /// <summary>
        /// Applies mutation to whole population and adapts range of mutation.
        /// </summary>
        /// <param name="population">Population on which mutation will operate.</param>
        public override void Apply(Population<T> population)
        {
            TryAdaptMutationRange();
            while (population.Children.Count < _childrenCount)
            {
                Individual<T> selectedParent = population.Parents.ElementAt(_random.Next(0, population.Parents.Count));
                Individual<T> newIndividual =
                    new Individual<T>(MutationLogicWithRange.Mutate(selectedParent.Chromosome));
                UpdateStatistic(population, newIndividual, selectedParent);
                population.Children.Add(newIndividual);
            }
        }
    }
}
