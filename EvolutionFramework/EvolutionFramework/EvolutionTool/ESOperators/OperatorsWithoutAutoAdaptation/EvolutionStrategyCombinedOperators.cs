﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements evolution operator (combined crossover and mutation) for evolution strategy.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class EvolutionStrategyCombinedOperators<T> : IEvolutionTool<T>
    {
        private readonly IRandom _random;
        private readonly ICrossOverLogic<T> _crossOverLogic;
        private readonly IMutationLogic<T> _mutationLogic;
        private readonly int _childrenCount;

        /// <summary>
        /// Constructor of evolution strategy operator (combined crossover and mutation).
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        /// <param name="mutationLogic">Logic of mutation.</param>
        /// <param name="crossOverLogic">Logic of crossover.</param> 
        /// <param name="childrenCount">Count of children to producee.</param>
        public EvolutionStrategyCombinedOperators(IRandom random, ICrossOverLogic<T> crossOverLogic, IMutationLogic<T> mutationLogic, int childrenCount)
        {
            _random = random;
            _crossOverLogic = crossOverLogic;
            _mutationLogic = mutationLogic;
            _childrenCount = childrenCount;
        }

        /// <summary>
        /// Applies evolution operator to whole population and adapts range of mutation.
        /// </summary>
        /// <param name="population">Population on which evolution operator will operate.</param>
        public void Apply(Population<T> population)
        {
            List<Individual<T>> children = new List<Individual<T>>();
            
            //CrossOver
            while (children.Count < _childrenCount)
            {
                List<Individual<T>> selectedParent = new List<Individual<T>>();
                for (int index = 0; index < _crossOverLogic.ParentCount; index++)
                {
                    selectedParent.Add(population.Parents.ElementAt(_random.Next(0, population.Parents.Count)));
                }

                children.AddRange(_crossOverLogic
                    .CrossOver(selectedParent.Select(parent => parent.Chromosome).ToList())
                    .Select(child => new Individual<T>(child)));
            }

            children.RemoveRange(_childrenCount, children.Count - _childrenCount);
            
            //mutate
            foreach (Individual<T> child in children)
            {
                population.Children.Add(new Individual<T>(_mutationLogic.Mutate(child.Chromosome)));
            }
        }
    }
}
