﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements mutation for evolution strategy.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class EvolutionStrategyMutation<T> : IEvolutionTool<T>
    {
        private readonly IRandom _random;
        private readonly IMutationLogic<T> _mutationLogic;
        private readonly int _childrenCount;

        /// <summary>
        /// Constructor of evolution strategy mutation.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        /// <param name="mutationLogic">Logic of mutation.</param>
        /// <param name="childrenCount">Count of children to producee.</param>
        public EvolutionStrategyMutation(IRandom random, IMutationLogic<T> mutationLogic, int childrenCount)
        {
            _random = random;
            _mutationLogic = mutationLogic;
            _childrenCount = childrenCount;
        }

        /// <summary>
        /// Applies mutation for to whole population.
        /// </summary>
        /// <param name="population">Population on which mutation will operate.</param>
        public void Apply(Population<T> population)
        {
            while (population.Children.Count < _childrenCount)
            {
                Individual<T> selectedParent = population.Parents.ElementAt(_random.Next(0, population.Parents.Count));
                population.Children.Add(new Individual<T>(_mutationLogic.Mutate(selectedParent.Chromosome)));
            }
        }
    }
}
