﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;
using Utils.Extensions;

namespace EvolutionFramework
{
    /// <summary>
    /// Clas wihch impelements genetic crossover.
    /// </summary>
    /// <typeparam name="T">Type of chromosome</typeparam>
    public class GeneticCrossOver<T> : IEvolutionTool<T>
    {
        private readonly ICrossOverLogic<T> _crossOverLogic;

        /// <summary>
        /// Class for generating random number.
        /// </summary>
        protected IRandom Random { get; set; }
        
        private readonly double _crossOverProbability;


        /// <summary>
        /// Constructor of genetic crossover.
        /// </summary>
        /// <param name="crossOverLogic"> </param>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="crossOverProbability">Probability of applying crossover</param>
        public GeneticCrossOver(ICrossOverLogic<T> crossOverLogic, IRandom random, double crossOverProbability)
        {
            _crossOverProbability = crossOverProbability;
            Random = random;
            _crossOverLogic = crossOverLogic;
        }

        /// <summary>
        /// Applies crossover to whole population.
        /// </summary>
        /// <param name="population">Population on which crossover will operate.</param>
        public virtual void Apply(Population<T> population)
        {
            List<Individual<T>> children = new List<Individual<T>>();

            foreach (IEnumerable<Individual<T>> parent in population.Parents.SplitIntoList(_crossOverLogic.ParentCount))
            {
                if (Random.NextDouble() <= _crossOverProbability)
                {
                    IEnumerable<Individual<T>> newIndividuals = 
                        _crossOverLogic.CrossOver(parent.Select(i => i.Chromosome).ToList()).Select(chromosome => new Individual<T>(chromosome));
                    children.AddRange(newIndividuals);
                }
            }

            population.AddChildren(children);
        }
    }
}
