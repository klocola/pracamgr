﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements one point crossover.
    /// </summary>
    public class OnePointCrossOver<T> : GeneticCrossOver<T[]>
    {
        /// <summary>
        /// Constructor of one point crossover.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="crossOverProbability">Probability of applying crossover</param>
        public OnePointCrossOver(IRandom random, double crossOverProbability) 
            : base(new OnePointCrossOverLogic<T>(random), random, crossOverProbability)
        {
        }
    }
}
