﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements line crossover.
    /// </summary>
    public class LineCrossOver : GeneticCrossOver<double[]>
    {
        /// <summary>
        /// Constructor of line crossover.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="crossOverProbability">Probability of applying crossover</param>
        public LineCrossOver(IRandom random, double crossOverProbability) 
            : base(new LineCrossOverLogic(random), random, crossOverProbability)
        {
        }
    }
}
