﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements two point crossover.
    /// </summary>
    public class TwoPointCrossOver<T> : GeneticCrossOver<T[]>
    {
        /// <summary>
        /// Constructor of two point crossover.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="crossOverProbability">Probability of applying crossover</param>
        public TwoPointCrossOver(IRandom random, double crossOverProbability) 
            : base(new TwoPointCrossOverLogic<T>(random), random, crossOverProbability)
        {
        }
    }
}
