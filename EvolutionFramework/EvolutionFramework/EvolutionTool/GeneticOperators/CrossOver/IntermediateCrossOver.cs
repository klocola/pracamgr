﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements intermediate crossover.
    /// </summary>
    public class IntermediateCrossOver : GeneticCrossOver<double[]>
    {
        /// <summary>
        /// Constructor of intermediate crossover.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="crossOverProbability">Probability of applying crossover</param>
        public IntermediateCrossOver(IRandom random, double crossOverProbability) 
            : base(new IntermediateCrossOverLogic(random), random, crossOverProbability)
        {
        }
    }
}
