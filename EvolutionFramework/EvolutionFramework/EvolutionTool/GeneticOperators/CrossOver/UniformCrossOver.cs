﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements uniform crossover.
    /// </summary>
    public class UniformCrossOver<T> : GeneticCrossOver<T[]>
    {
        /// <summary>
        /// Constructor of uniform crossover.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="crossOverProbability">Probability of applying crossover</param>
        public UniformCrossOver(IRandom random, double crossOverProbability)
            : base(new UniformCrossOverLogic<T>(random), random, crossOverProbability)
        {
        }
    }
}
