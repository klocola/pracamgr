﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements partially matched crossover.
    /// </summary>
    public class PartiallyMatchedCrossOver<T> : GeneticCrossOver<T[]> where T : IComparable
    {
        /// <summary>
        /// Constructor of partially matched crossover.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="crossOverProbability">Probability of applying crossover</param>
        public PartiallyMatchedCrossOver(IRandom random, double crossOverProbability) 
            : base(new PartiallyMatchedCrossOverLogic<T>(random), random, crossOverProbability)
        {
        }
    }
}
