﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    //Class which implements genetic mutation (change bit to opposed) on binary array. 
    /// </summary>
    public class BinaryChangeMutation : GeneticMutation<int[]>
    {
        /// <summary>
        /// Constructor of binary change genetic mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="mutationProbability">Propbability of applying mutation.</param>
        /// <param name="changeBitProbability">Propability of change bit on single gene.</param>
        public BinaryChangeMutation(IRandom random, double mutationProbability, double changeBitProbability) 
            : base(new BinaryChangeMutationLogic(random, changeBitProbability), random, mutationProbability)
        {
        }
    }
}
