﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements genetic mutation (change bit on aleatory one) on binary array. 
    /// </summary>
    public class BinaryRandomMutation : GeneticMutation<int[]>
    {
        /// <summary>
        /// Constructor of binary random genetic mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="mutationProbability">Propbability of applying mutation.</param>
        /// <param name="changeBitProbability">Propability of change bit on single gene.</param>
        public BinaryRandomMutation(IRandom random, double mutationProbability, double changeBitProbability) 
            : base(new BinaryRandomMutationLogic(random, changeBitProbability), random, mutationProbability)
        {
        }
    }
}
