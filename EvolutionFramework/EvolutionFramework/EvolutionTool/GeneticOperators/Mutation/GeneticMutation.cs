﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Clas wihch impelements genetic mutation.
    /// </summary>
    /// <typeparam name="T">Type of chromosome</typeparam>
    public class GeneticMutation<T> : IEvolutionTool<T>
    {
        /// <summary>
        /// Class for generating random number.
        /// </summary>
        protected IRandom Random { get; set; }

        private readonly double _mutationProbability;
        private readonly IMutationLogic<T> _mutator;

        /// <summary>
        /// Constructor of genetic mutation.
        /// </summary>
        /// <param name="mutator"></param>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="mutationProbability">Propability of applying mutation.</param>
        public GeneticMutation(IMutationLogic<T> mutator, IRandom random, double mutationProbability)
        {
            _mutationProbability = mutationProbability;
            Random = random;
            _mutator = mutator;
        }

        /// <summary>
        /// Applaies mutation to whole population.
        /// </summary>
        /// <param name="population">Population on which mutation will operate.</param>
        public virtual void Apply(Population<T> population)
        {
            IEnumerable<Individual<T>> children = from parent in population.Parents 
                            where Random.NextDouble() <= _mutationProbability 
                            select _mutator.Mutate(parent.Chromosome) into newChromosome 
                            select new Individual<T>(newChromosome);

            population.AddChildren(children);
        }
    }
}
