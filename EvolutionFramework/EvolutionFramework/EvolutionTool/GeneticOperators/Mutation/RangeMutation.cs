﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements range mutation.
    /// </summary>
    public class RangeMutation : GeneticMutation<double[]>
    {
        /// <summary>
        /// Constructor of range mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="mutationProbability">Propability of applying mutation.</param>
        /// <param name="changeBitProbability">Propability of change bit on single gene.</param>
        /// <param name="range">Range of mutation on single gene.</param>
        public RangeMutation(IRandom random, double mutationProbability, double changeBitProbability, double range)
            : base(new RangeMutationLogic(random, changeBitProbability, range), random, mutationProbability)
        {
        }
    }
}
