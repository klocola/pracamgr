﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements inversion mutation.
    /// </summary>
    public class InversionMutation<T> : GeneticMutation<T[]>
    {
        /// <summary>
        /// Constructor of inversion genetic mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="mutationProbability">Propbability of applying mutation.</param>
        public InversionMutation(IRandom random, double mutationProbability) 
           : base(new InversionMutationLogic<T>(random), random, mutationProbability)
        {
        }
    }
}
