﻿namespace EvolutionFramework
{
    public interface IEvolutionTool<T>
    {
        /// <summary>
        /// Applies evolution tool to population.
        /// </summary>
        /// <param name="population">Population on which evolution tool will operate.</param>
        void Apply(Population<T> population);
    }
}
