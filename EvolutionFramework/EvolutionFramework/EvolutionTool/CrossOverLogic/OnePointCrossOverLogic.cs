﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements one point crossover  logic.
    /// </summary>
    /// <typeparam name="T">Type of gene in chromosome.</typeparam>
    public class OnePointCrossOverLogic<T> : ICrossOverLogic<T[]>
    {
        private readonly IRandom _random;

        /// <summary>
        /// Count of parents for crossover method
        /// </summary>
        public int ParentCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Constructor of one point crossover logic.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        public OnePointCrossOverLogic(IRandom random)
        {
            _random = random;
        }

        /// <summary>
        /// Applies single crossover on parents and generates children.
        /// </summary>
        /// <param name="parents">Parents selected for crossover.</param>
        /// <returns>Generated children in single crossover.</returns>
        public ICollection<T[]> CrossOver(ICollection<T[]> parents)
        {
            List<T[]> children = new List<T[]>();
            
            T[] parent1 = parents.ElementAt(0);
            T[] parent2 = parents.ElementAt(1);
            if (parent1.Length == parent2.Length)
            {
                int index = _random.Next(0, parent1.Length);
                T[] children1 = new T[parent1.Length];
                T[] children2 = new T[parent1.Length];

                Array.Copy(parent1, children1, index);
                Array.Copy(parent2, children2, index);

                Array.Copy(parent1, index, children2, index, parent1.Length - index);
                Array.Copy(parent2, index, children1, index, parent2.Length - index);

                children.Add(children1);
                children.Add(children2);
            }

            return children;
        }
    }
}
