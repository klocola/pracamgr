﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements line crossover logic.
    /// </summary>
    public class LineCrossOverLogic : IntermediateCrossOverLogic
    {
        private double? _scalingFactor;

        /// <summary>
        /// Scaling factor of genes.
        /// </summary>
        protected override double ScalingFactor
        {
            get
            {
                if (!_scalingFactor.HasValue)
                {
                    _scalingFactor = base.ScalingFactor;
                }

                return _scalingFactor.Value;
            }
        }

        /// <summary>
        /// Constructor of line crossover logic with default area of offspring.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        public LineCrossOverLogic(IRandom random) 
            : base(random)
        {
        }
    }
}
