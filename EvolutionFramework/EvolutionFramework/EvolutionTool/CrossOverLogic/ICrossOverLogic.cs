﻿using System.Collections.Generic;
using System.Linq;
using Utils;
using Utils.Extensions;

namespace EvolutionFramework
{
    /// <summary>
    /// Interface for logic of crossover.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public interface ICrossOverLogic<T>
    {
        /// <summary>
        /// Count of parents for crossover method
        /// </summary>
        int ParentCount { get; }

        /// <summary>
        /// Method which applies single crossover for selected parents.
        /// </summary>
        /// <param name="parents">Parents selected for crossover.</param>
        /// <returns>Generated children in single crossover.</returns>
        ICollection<T> CrossOver(ICollection<T> parents);
    }
}
