﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements partially matched crossover logic.
    /// </summary>
    /// <typeparam name="T">Type of gene in chromosome.</typeparam>
    public class TwoPointCrossOverLogic<T> : ICrossOverLogic<T[]>
    {
        private readonly IRandom _random;

        /// <summary>
        /// Count of parents for crossover method
        /// </summary>
        public int ParentCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Constructor of two point crossover logic.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        public TwoPointCrossOverLogic(IRandom random)
        {
            _random = random;
        }

        /// <summary>
        /// Applies single crossover on parents and generates children.
        /// </summary>
        /// <param name="parents">Parents selected for crossover.</param>
        /// <returns>Generated children in single crossover.</returns>
        public ICollection<T[]> CrossOver(ICollection<T[]> parents)
        {
            List<T[]> children = new List<T[]>();

            T[] parent1 = parents.ElementAt(0);
            T[] parent2 = parents.ElementAt(1);
            if (parent1.Length == parent2.Length)
            {
                int index1 = _random.Next(0, parent1.Length);
                int index2 = _random.Next(index1, parent1.Length);
                T[] children1 = new T[parent1.Length];
                T[] children2 = new T[parent1.Length];

                Array.Copy(parent1, children1, index1);
                Array.Copy(parent2, children2, index1);

                Array.Copy(parent1, index1, children2, index1, index2 - index1);
                Array.Copy(parent2, index1, children1, index1, index2 - index1);

                Array.Copy(parent1, index2, children1, index2, parent1.Length - index2);
                Array.Copy(parent2, index2, children2, index2, parent2.Length - index2);

                children.Add(children1);
                children.Add(children2);
            }

            return children;
        }
    }
}
