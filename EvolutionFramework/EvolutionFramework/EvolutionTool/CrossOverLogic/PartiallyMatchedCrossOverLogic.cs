﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;
using Utils.Extensions;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements partially matched crossover logic.
    /// </summary>
    /// <typeparam name="T">Type of gene in chrmosome.</typeparam>
    public class PartiallyMatchedCrossOverLogic<T> : ICrossOverLogic<T[]> where T : IComparable
    {
        private readonly IRandom _random;

        /// <summary>
        /// Count of parents for crossover method
        /// </summary>
        public int ParentCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Constructor of partially matched crossover logic.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        public PartiallyMatchedCrossOverLogic(IRandom random)
        {
            _random = random;
        }

        private static void RepairPermutation(T[] parent1, T[] parent2, int startIndex, int endIndex, T[] children1, T[] children2)
        {
            for (int index = startIndex; index < endIndex; index++)
            {
                int changeIndex = index;

                while (children1.Where(x => x.Equals(children1[index])).Count() > 1)
                {
                    changeIndex = parent2.IndexOf(x => x.Equals(children1[index]));
                    children1[index] = parent1[changeIndex];
                }

                changeIndex = index;
                while (children2.Where(x => x.Equals(children2[index])).Count() > 1)
                {
                    changeIndex = parent1.IndexOf(x => x.Equals(children2[index]));
                    children2[index] = parent2[changeIndex];
                }
            }
        }

        /// <summary>
        /// Applies single crossover on parents and generates children.
        /// </summary>
        /// <param name="parents">Parents selected for crossover.</param>
        /// <returns>Generated children in single crossover.</returns>
        public ICollection<T[]> CrossOver(ICollection<T[]> parents)
        {
            List<T[]> children = new List<T[]>();

            T[] parent1 = parents.ElementAt(0);
            T[] parent2 = parents.ElementAt(1);
            if (parent1.Length == parent2.Length)
            {
                int index1 = _random.Next(0, parent1.Length);
                int index2 = _random.Next(index1, parent1.Length);

                T[] children1 = (T[])parent1.Clone();
                T[] children2 = (T[])parent2.Clone();
                
                for (int i = index1; i < index2; i++)
                {
                    children1[i] = parent2[i];
                    children2[i] = parent1[i];
                }

                Array.Copy(parent2, index1, children1, index1, index2 - index1);
                Array.Copy(parent1, index1, children2, index1, index2 - index1);

                RepairPermutation(parent1, parent2, 0, index1, children1, children2);
                RepairPermutation(parent1, parent2, index2, parent2.Length, children1, children2);

                children.Add(children1);
                children.Add(children2);
            }

            return children;
        }
    }
}
