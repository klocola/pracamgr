﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements uniform crossover logic.
    /// </summary>
    /// <typeparam name="T">Type of gene in chromosome.</typeparam>
    public class UniformCrossOverLogic<T> : ICrossOverLogic<T[]>
    {
        private readonly IRandom _random;
        private readonly double _swapProbability = 0.5;

        /// <summary>
        /// Count of parents for crossover method
        /// </summary>
        public int ParentCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Constructor of uniform crossover.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        public UniformCrossOverLogic(IRandom random)
        {
            _random = random;
        }

        /// <summary>
        /// Constructor of uniform crossover logic.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        /// <param name="swapProbability">Popability of swap selected gene in chromosome.</param>
        public UniformCrossOverLogic(IRandom random, double swapProbability)
        {
            _random = random;
            _swapProbability = swapProbability;
        }

        /// <summary>
        /// Applies single crossover on parents and generates children.
        /// </summary>
        /// <param name="parents">Parents selected for crossover.</param>
        /// <returns>Generated children in single crossover.</returns>
        public ICollection<T[]> CrossOver(ICollection<T[]> parents)
        {
            List<T[]> children = new List<T[]>();

            T[] parent1 = parents.ElementAt(0);
            T[] parent2 = parents.ElementAt(1);
            if (parent1.Length == parent2.Length)
            {
                T[] children1 = (T[])parent1.Clone();
                T[] children2 = (T[])parent2.Clone();

                for (int i = 0; i < parent1.Length; i++)
                {
                    if (_random.NextDouble() < _swapProbability)
                    {
                        children1[i] = parent2[i];
                        children2[i] = parent1[i];
                    }
                }

                children.Add(children1);
                children.Add(children2);
            }

            return children;
        }
    }
}
