﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements intermadiate crossover logic.
    /// </summary>
    public class IntermediateCrossOverLogic : ICrossOverLogic<double[]>
    {
        private readonly double _offspringArea = 0.25;
        private readonly IRandom _random;

        /// <summary>
        /// Count of parents for crossover method
        /// </summary>
        public int ParentCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Scaling factor of genes.
        /// </summary>
        protected virtual double ScalingFactor
        {
            get { return (_random.NextDouble() * (1 + _offspringArea)) - _offspringArea; }
        }

        /// <summary>
        /// Constructor of intermediate crossover logic.
        /// </summary>
        /// <param name="random">Class for generating random numer.</param>
        /// <param name="offspringArea">Area of generated offspring.</param>
        public IntermediateCrossOverLogic(IRandom random, double offspringArea)
        {
            _random = random;
            _offspringArea = offspringArea;
        }

        /// <summary>
        /// Constructor of intermediate crossover with default area of offspring.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        public IntermediateCrossOverLogic(IRandom random)
        {
            _random = random;
        }

        /// <summary>
        /// Applies single crossover on parents and generates children.
        /// </summary>
        /// <param name="parents">Parents selected for crossover.</param>
        /// <returns>Generated children in single crossover.</returns>
        public ICollection<double[]> CrossOver(ICollection<double[]> parents)
        {
            List<double[]> children = new List<double[]>();

            double[] parent1 = parents.ElementAt(0);
            double[] parent2 = parents.ElementAt(1);
            if (parent1.Length == parent2.Length)
            {
                double[] child = new double[parent1.Length];

                for (int i = 0; i < parent1.Length; i++)
                {
                    double a = ScalingFactor;
                    child[i] = parent1[i]*a + parent2[i]*(1 - a);
                }

                children.Add(child);
            }

            return children;
        }
    }
}
