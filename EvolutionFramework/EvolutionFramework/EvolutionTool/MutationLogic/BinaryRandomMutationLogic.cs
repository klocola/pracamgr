﻿using System;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements mutation logic (change bit on aleatory one) on binary array. 
    /// </summary>
    public class BinaryRandomMutationLogic : IMutationLogic<int[]>
    {
        private readonly IRandom _random;
        private readonly double _changeBitProbability;

        /// <summary>
        /// Constructor of binary random mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="changeBitProbability">Propability of change bit on single gene.</param>
        public BinaryRandomMutationLogic(IRandom random, double changeBitProbability)
        {
            _random = random;
            _changeBitProbability = changeBitProbability;
        }

        /// <summary>
        /// Mutates selected chromosome.
        /// </summary>
        /// <param name="chromosome">Chromosome which will be mutated.</param>
        /// <returns>Generated child in single mutation.</returns>
        public int[] Mutate(int[] chromosome)
        {
            int[] newChromosome = (int[])chromosome.Clone();
            for (int i = 0; i < newChromosome.Length; i++)
            {
                if (_random.NextDouble() <= _changeBitProbability)
                {
                    newChromosome[i] = (int)Math.Round(_random.NextDouble());
                }
            }

            return newChromosome;
        }
    }
}
