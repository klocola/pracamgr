﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements range mutation logic. 
    /// </summary>
    public class RangeMutationLogic : IMutationLogicWithRange<double[]>
    {
        private readonly IRandom _random;
        private readonly double _changeBitProbability;
        
        /// <summary>
        /// Range of mutation on single gene.
        /// </summary>
        public double Range { get; set; }

        /// <summary>
        /// Mess wich will be applied to gene.
        /// </summary>
        private double Mess
        {
            get
            {
                return (_random.NextDouble() - 0.5) * Range * 2.0f;
            }
        }

        /// <summary>
        /// Constructor of range mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="changeBitProbability">Propability of change bit on single gene.</param>
        /// <param name="range">Range of mutation on single gene.</param>
        public RangeMutationLogic(IRandom random, double changeBitProbability, double range)
        {
            _random = random;
            _changeBitProbability = changeBitProbability;
            Range = range;
        }

        /// <summary>
        /// Mutates selected chromosome.
        /// </summary>
        /// <param name="chromosome">Chromosome which will be mutated.</param>
        /// <returns>Generated child in single mutation.</returns>
        public double[] Mutate(double[] chromosome)
        {
            double[] newChromosome = (double[])chromosome.Clone();
            for (int i = 0; i < newChromosome.Length; i++)
            {
                if (_random.NextDouble() <= _changeBitProbability)
                {
                    newChromosome[i] = chromosome[i] + Mess;
                }
            }

            return newChromosome;
        }
    }
}
