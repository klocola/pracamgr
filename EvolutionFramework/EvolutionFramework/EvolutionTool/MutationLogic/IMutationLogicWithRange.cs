﻿using System.Collections.Generic;
using System.Linq;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Interface for logic of mutation.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public interface IMutationLogicWithRange<T> : IMutationLogic<T>
    {
        /// <summary>
        /// Range of mutation on single gene.
        /// </summary>
        double Range { get; set; }
    }
}

