﻿using System.Collections.Generic;
using System.Linq;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Interface for logic of mutation.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public interface IMutationLogic<T>
    {
        /// <summary>
        /// Method which applies single mutation 
        /// </summary>
        /// <param name="chromosome">Chromosome which will be mutated.</param>
        /// <returns>Generated child in single mutation.</returns>
       T Mutate(T chromosome);
    }
}
