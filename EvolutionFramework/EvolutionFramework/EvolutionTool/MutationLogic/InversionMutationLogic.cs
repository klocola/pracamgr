﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements inversion mutation logic. 
    /// </summary>
    /// <typeparam name="T">Type of gene in chromosome.</typeparam>
    public class InversionMutationLogic<T> : IMutationLogic<T[]>
    {
        private readonly IRandom _random;

        /// <summary>
        /// Constructor of inversion mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        public InversionMutationLogic(IRandom random)
        {
            _random = random;
        }

        /// <summary>
        /// Mutates selected chromosome.
        /// </summary>
        /// <param name="chromosome">Chromosome which will be mutated.</param>
        /// <returns>Generated child in single mutation.</returns>
        public T[] Mutate(T[] chromosome)
        {
            T[] newChromosome = (T[])chromosome.Clone();
            
            int index1 = _random.Next(0, chromosome.Length);
            int index2 = _random.Next(0, chromosome.Length);

            newChromosome[index1] = chromosome[index2]; 
            newChromosome[index2] = chromosome[index1];

            return newChromosome;
        }
    }
}
