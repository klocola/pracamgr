﻿using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements mutation logic (change bit to opposed) on binary array. 
    /// </summary>
    public class BinaryChangeMutationLogic : IMutationLogic<int[]>
    {
        private readonly IRandom _random;
        private readonly double _changeBitProbability;

        /// <summary>
        /// Constructor of binary change mutation.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="changeBitProbability">Propability of change bit on single gene.</param>
        public BinaryChangeMutationLogic(IRandom random, double changeBitProbability)
        {
            _random = random;
            _changeBitProbability = changeBitProbability;
        }

        /// <summary>
        /// Mutates selected chromosome.
        /// </summary>
        /// <param name="chromosome">Chromosome which will be mutated.</param>
        /// <returns>Generated child in single mutation.</returns>
        public int[] Mutate(int[] chromosome)
        {
            int[] newChromosome = (int[])chromosome.Clone();
            for (int i = 0; i < newChromosome.Length; i++)
            {
                if (_random.NextDouble() <= _changeBitProbability)
                {
                    newChromosome[i] = chromosome[i] == 0 ? 1 : 0;
                }
            }

            return newChromosome;
        }
    }
}
