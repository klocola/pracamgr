﻿namespace EvolutionFramework
{
    /// <summary>
    /// Abstract class for selection.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AbstractSelection<T> : IEvolutionTool<T>
    {
        protected int TargetPopulationSize { get; set; }
        protected ISelector<T> Selector { get; set; }

        /// <summary>
        /// Constructor of selection.
        /// </summary>
        /// <param name="targetPopulationSize">Target size of new population.</param>
        /// <param name="selector">Class which will select new population.</param>
        protected AbstractSelection(int targetPopulationSize, ISelector<T> selector)
        {
            TargetPopulationSize = targetPopulationSize;
            Selector = selector;
        }

        /// <summary>
        /// Abstract method which select new population and replaces old one.
        /// </summary>
        /// <param name="population"></param>
        protected abstract void Select(Population<T> population);

        /// <summary>
        /// Abstract method which select new population, replaces old one and clears children.
        /// </summary>
        /// <param name="population">Population on which evolution selection will operate.</param>
        public virtual void Apply(Population<T> population)
        {
            Select(population);
            population.ClearChildren();
        }
    }
}
