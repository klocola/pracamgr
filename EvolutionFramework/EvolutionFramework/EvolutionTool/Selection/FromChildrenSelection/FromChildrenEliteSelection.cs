﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements elite selection (select new individuals from children).
    /// </summary>
    /// <typeparam name="T">Type of chromosome</typeparam>
    public class FromChildrenEliteSelection<T> : FromChildrenSelection<T>
    {
        /// <summary>
        /// Constructor of selection which select according to elite selection.
        /// </summary>
        /// <param name="targetPopulationSize">Target size of new population.</param>
        public FromChildrenEliteSelection(int targetPopulationSize)
            : base(targetPopulationSize, new EliteSelector<T>())
        {
        }
    }
}
