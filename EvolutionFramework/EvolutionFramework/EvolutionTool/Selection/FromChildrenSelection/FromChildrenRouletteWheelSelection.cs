﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements roulette wheel (select new individuals from children).
    /// </summary>
    /// <typeparam name="T">Type of chromosome</typeparam>
    public class FromChildrenRouletteWheelSelection<T> : FromChildrenSelection<T>
    {
        /// <summary>
        /// Constructor of selection which select according to roulette wheel selection.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="targetPopulationSize">Target size of new population.</param>
        public FromChildrenRouletteWheelSelection(IRandom random, int targetPopulationSize)
            : base(targetPopulationSize, new RouletteWheelSelector<T>(random))
        {
        }
    }
}
