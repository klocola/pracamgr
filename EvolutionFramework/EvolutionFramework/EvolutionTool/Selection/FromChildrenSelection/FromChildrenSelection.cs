﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements selection (select new individuals from children).
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class FromChildrenSelection<T> : AbstractSelection<T>
    {
        /// <summary>
        /// Constructor of selection.
        /// </summary>
        /// <param name="targetPopulationSize">Target size of new population.</param>
        /// <param name="selector">Class which will select new population.</param>
        public FromChildrenSelection(int targetPopulationSize, ISelector<T> selector) 
            : base(targetPopulationSize, selector)
        {
        }

        /// <summary>
        /// Selects new population and replaces old one.
        /// </summary>
        /// <param name="population">Population on which evolution selection will operate.</param>
        protected override void Select(Population<T> population)
        {
            population.Parents = Selector.Select(population.Children, TargetPopulationSize).ToList();
        }
    }
}
