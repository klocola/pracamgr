﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements elite selection logic.
    /// </summary>
    /// <typeparam name="T">Type of chromosome</typeparam>
    public class EliteSelector<T> : ISelector<T>
    {
        /// <summary>
        /// Selects best individuals.
        /// </summary>
        /// <param name="individuals">Collection of individuals from which new individuals will be selected.</param>
        /// <param name="targetSize">Target size of returned collection of individuals.</param>
        /// <returns>Selected individuals.</returns>
        public IEnumerable<Individual<T>> Select(IEnumerable<Individual<T>> individuals, int targetSize)
        {
            individuals = individuals.OrderByDescending(individual => individual.Fitness).ToList();
            return individuals.Take(targetSize);
        }
    }
}
