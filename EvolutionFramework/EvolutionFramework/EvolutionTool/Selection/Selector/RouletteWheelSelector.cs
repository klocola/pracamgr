﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements roulette wheel selection logic.
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class RouletteWheelSelector<T> : ISelector<T>
    {
        private readonly IRandom _random;

        /// <summary>
        /// Constructor of roulette wheel selector.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        public RouletteWheelSelector(IRandom random)
        {
            _random = random;
        }

        /// <summary>
        /// Selects individuals according to roulette wheel selection algorithm.
        /// </summary>
        /// <param name="individuals">Collection of individuals from which new individuals will be selected.</param>
        /// <param name="targetSize">Target size of returned collection of individuals.</param>
        /// <returns>Selected individuals.</returns>
        public IEnumerable<Individual<T>> Select(IEnumerable<Individual<T>> individuals, int targetSize)
        {
            if (individuals != null)
            {
                double sumFitness = individuals.Sum(individual => individual.Fitness.Value);
                
                for (int i = 0; i < targetSize; i++)
                {
                    double random = sumFitness * _random.NextDouble();
                    double sum = 0;
                    int index = -1;
                    while (sum < random)
                    {
                        index++;
                        sum += individuals.ElementAt(index).Fitness.Value;
                    }

                    yield return individuals.ElementAt(index);
                }
            }
        }
    }
}
