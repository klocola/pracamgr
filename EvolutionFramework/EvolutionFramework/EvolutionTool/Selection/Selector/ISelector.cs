﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvolutionFramework
{
    /// <summary>
    /// Interface for selector.
    /// </summary>
    /// <typeparam name="T">Type od chromosome.</typeparam>
    public interface ISelector<T>
    {
        /// <summary>
        /// Selects individuals.
        /// </summary>
        /// <param name="individuals">Collection of individuals from which new individuals will be selected.</param>
        /// <param name="targetSize">Target size of returned collection of individuals.</param>
        /// <returns>Selected individuals.</returns>
        IEnumerable<Individual<T>> Select(IEnumerable<Individual<T>> individuals, int targetSize);
    }
}
