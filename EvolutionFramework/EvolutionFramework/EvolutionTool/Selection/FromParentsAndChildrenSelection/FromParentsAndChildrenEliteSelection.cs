﻿using System.Collections.Generic;
using System.Linq;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements elite selection (select new individuals from parents and children).
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class FromParentsAndChildrenEliteSelection<T> : FromParentsAndChildrenSelection<T>
    {
        /// <summary>
        /// Constructor of elite selection.
        /// </summary>
        /// <param name="targetPopulationSize">Target size of new popualtion.</param>
        public FromParentsAndChildrenEliteSelection(int targetPopulationSize) 
            : base(targetPopulationSize, new EliteSelector<T>())
        {
        }
    }
}
