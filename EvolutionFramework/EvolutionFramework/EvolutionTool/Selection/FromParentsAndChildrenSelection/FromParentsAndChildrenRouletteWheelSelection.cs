﻿using System.Collections.Generic;
using System.Linq;
using Utils;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which implements roulette wheel selection  (select new individuals from parents and children).
    /// </summary>
    /// <typeparam name="T">Type of chromosome.</typeparam>
    public class FromParentsAndChildrenRouletteWheelSelection<T> : FromParentsAndChildrenSelection<T>
    {
        /// <summary>
        /// Constructor of roulette wheel selection.
        /// </summary>
        /// <param name="random">Class for generating random number.</param>
        /// <param name="targetPopulationSize">Target size of new popualtion.</param>
        public FromParentsAndChildrenRouletteWheelSelection(IRandom random, int targetPopulationSize) 
            : base(targetPopulationSize, new RouletteWheelSelector<T>(random))
        {
        }
    }
}
