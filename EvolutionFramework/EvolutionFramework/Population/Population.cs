﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utils.Extensions;

namespace EvolutionFramework
{
    /// <summary>
    /// Class which represents population.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Population<T>
    {
        /// <summary>
        /// Start time of evaluation.
        /// </summary>
        public DateTime StartTime { get; private set; }

        /// <summary>
        /// Number af actual generation.
        /// </summary>
        public int GenerationNumber { get; set; }

        #region Parents

        /// <summary>
        /// Colleciton of individuals in population.
        /// </summary>
        public List<Individual<T>> Parents { get; set; }

        /// <summary>
        /// Parent with best fitness.
        /// </summary>
        public Individual<T> BestParent
        {
            get
            {
                return Parents.OfMax(individual => individual.Fitness.Value);
            }
        }

        /// <summary>
        /// The greatest fitness in population.
        /// </summary>
        public double BestParentFitness
        {
            get
            {
                return Parents.Max(individual => individual.Fitness.Value);
            }
        }

        #endregion

        #region Children

        /// <summary>
        /// Collection of generated children.
        /// </summary>
        public List<Individual<T>> Children { get; set; }

        /// <summary>
        /// Child with best fitness.
        /// </summary>
        public Individual<T> BestChild
        {
            get
            {
                return Children.OfMax(child => child.Fitness.Value);
            }
        }

        /// <summary>
        /// The greatest fitness in collection of child.
        /// </summary>
        public double BestChildFitness
        {
            get
            {
                return Children.Max(child => child.Fitness.Value);
            }
        }

        #endregion

        /// <summary>
        /// Constructor odf population.
        /// </summary>
        /// <param name="individuals">Collection of individuals which belongs to population.</param>
        public Population(IEnumerable<Individual<T>> individuals)
        {
            Parents = individuals.ToList();
            Children = new List<Individual<T>>();
            StartTime = DateTime.UtcNow;
        }

        #region Parents

        /// <summary>
        /// Adds individual to popualtion.
        /// </summary>
        /// <param name="individual">Parent to add.</param>
        public void AddParent(Individual<T> individual)
        {
            Parents.Add(individual);
        }

        /// <summary>
        /// Add collection of individuals to population.
        /// </summary>
        /// <param name="individuals">Collection of individuals to add.</param>
        public void AddParents(IEnumerable<Individual<T>> individuals)
        {
            Parents.AddRange(individuals);
        }

        /// <summary>
        /// Removes specified collection of individuals from population. 
        /// </summary>
        /// <param name="individualsToRemove">Collection of individuals to remove.</param>
        public void RemoveParents(IEnumerable<Individual<T>> individualsToRemove)
        {
            Parents = Parents.Except(individualsToRemove).ToList();
        }

        #endregion

        #region Children

        /// <summary>
        /// Adds individual to collection of children.
        /// </summary>
        /// <param name="child">Chil to add.</param>
        public void AddChild(Individual<T> child)
        {
            Children.Add(child);
        }

        /// <summary>
        /// Add collection of infividuals to collection of child.
        /// </summary>
        /// <param name="children">Collection od individual to add.</param>
        public void AddChildren(IEnumerable<Individual<T>> children)
        {
            Children.AddRange(children);
        }

        /// <summary>
        /// Removes specified collection of individuala from collection of children. 
        /// </summary>
        /// <param name="childrenToRemove">Collection of individuals to remove.</param>
        public void RemoveChildren(IEnumerable<Individual<T>> childrenToRemove)
        {
            Children = Children.Except(childrenToRemove).ToList();
        }

        #endregion

        #region Merge

        /// <summary>
        /// Add collection of children to collection of individuals (population)
        /// </summary>
        public void AddChildrenToParents()
        {
            Parents.AddRange(Children);
        }

        /// <summary>
        /// Clear collection of children.
        /// </summary>
        public void ClearChildren()
        {
            Children = new List<Individual<T>>();
        }

        #endregion
    }
}
