﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;
using Utils;

namespace AckleyProblem
{
    public class Configuration : AbstractEvolutionStrategyConfiguration
    {
        private const int DefaultChromosomeSize = 30;
        private const int DefaultIndividualFactoryRange = 30;

        public override IFitness<double[]> Fitness
        {
            get { return new Fitness(); }
        }

        public override IIndividualsFactory<double[]> IndividualsFactory
        {
            get { return new DoubleArrayIndividualFactory(Random, DefaultChromosomeSize, DefaultIndividualFactoryRange); }
        }
    }
}
