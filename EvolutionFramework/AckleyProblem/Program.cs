﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;

namespace AckleyProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine<double[]> engine = new Engine<double[]>(new Configuration());
            engine.RunEvolution();
            Console.ReadLine();
        }
    }
}
