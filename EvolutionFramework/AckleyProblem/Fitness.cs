﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;

namespace AckleyProblem
{
    public class Fitness : AbstractFitness<double[]>
    {
        private readonly int n = 30;
        public override double CalculateFitness(Population<double[]> population, Individual<double[]> individual)
        {
            double d = 1.0/n;
            double value = -20*Math.Exp(-0.2*Math.Sqrt(d*SumPowers(individual))) -
                        Math.Exp(d*SumCos(individual)) + 20 + Math.E;

            return 1/value;
        }

        private double SumCos(Individual<double[]> individual)
        {
            return individual.Chromosome.Sum(x => Math.Cos( 2 * Math.PI * x));
        }

        private double SumPowers(Individual<double[]> individual)
        {
            return individual.Chromosome.Sum(x => x*x);
        }
    }
}
