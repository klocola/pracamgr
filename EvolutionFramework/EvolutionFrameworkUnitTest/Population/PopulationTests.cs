﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Extensions;

namespace EvolutionFrameworkUnitTest.Population
{
    [TestClass]
    public class PopulationTests
    {
        private Individual<string> _bestIndividual;
        private Individual<string> _worstIndividual;
        private Individual<string> _midIndividual;
        private Individual<string> _bestChild;
        private Individual<string> _worstChild;
        private Individual<string> _midChild;
        private Population<string> _population;

        [TestInitialize]
        public void TestInitialize()
        {
            _bestIndividual = new Individual<string>("_bestIndividual") { Fitness = 1 };
            _worstIndividual = new Individual<string>("_worstIndividual") { Fitness = 0.1 };
            _midIndividual = new Individual<string>("_midIndividual") { Fitness = 0.5 };
            _bestChild = new Individual<string>("_bestChild") { Fitness = 0.78 };
            _worstChild = new Individual<string>("_worstChild") { Fitness = 0 };
            _midChild = new Individual<string>("_midChild") { Fitness = 0.45 };
            List<Individual<string>> individuals = new List<Individual<string>> { _bestIndividual, _worstIndividual, _midIndividual };
            List<Individual<string>> children = new List<Individual<string>> { _bestChild, _midChild, _worstChild };
            _population = new Population<string>(individuals) { Children = children };
        }

        #region Individuals

        [TestMethod]
        public void WhenAddIndividualThenIndividualsContainsNewIndividual()
        {
            //Arrange
            Individual<string> newIndividual = new Individual<string>("new");

            //Act
            _population.AddParent(newIndividual);

            //Assert
            CollectionAssert.Contains(_population.Parents, newIndividual);
        }

        [TestMethod]
        public void WhenAddIndividualsThenIndividualsContainsNewIndividuals()
        {
            //Arrange
            Individual<string> newIndividual1 = new Individual<string>("new1");
            Individual<string> newIndividual2 = new Individual<string>("new2");
            List<Individual<string>> newIndividuals = new List<Individual<string>> { newIndividual1, newIndividual2 };
            
            //Act
            _population.AddParents(newIndividuals);

            //Assert
            CollectionAssert.Contains(_population.Parents, newIndividual1);
            CollectionAssert.Contains(_population.Parents, newIndividual2);
        }

        [TestMethod]
        public void WhenRemoveIndividualsThenIndividualsDoesNotContainsRemovedIndividual()
        {
            //Arrange
            List<Individual<string>> individualsToRemove = new List<Individual<string>> { _midIndividual, _worstIndividual };

            //Act
            _population.RemoveParents(individualsToRemove);

            //Assert
            CollectionAssert.DoesNotContain(_population.Parents, _midIndividual);
            CollectionAssert.DoesNotContain(_population.Parents, _worstIndividual);
        }

        [TestMethod]
        public void BestIndividualvoid()
        {
            //Arrange

            //Act
            Individual<string> besIndividual = _population.BestParent;

            //Assert
            Assert.AreEqual(_bestIndividual, besIndividual);
        }

        [TestMethod]
        public void BestIndividualFitnessTest()
        {
            //Arrange

            //Act
            double bestIndividualFitness = _population.BestParentFitness;

            //Assert
            Assert.AreEqual(_bestIndividual.Fitness, bestIndividualFitness);
        }

        #endregion

        #region Children

        [TestMethod]
        public void WhenAddChildThenChildrenContainsNewChild()
        {
            //Arrange
            Individual<string> newChild = new Individual<string>("new");

            //Act
            _population.AddChild(newChild);

            //Assert
            CollectionAssert.Contains(_population.Children, newChild);
        }

        [TestMethod]
        public void WhenAddChildrenThenChildrenContainsNewChildren()
        {
            //Arrange
            Individual<string> newChild1 = new Individual<string>("new1");
            Individual<string> newChild2 = new Individual<string>("new2");
            List<Individual<string>> newChildren = new List<Individual<string>> { newChild1, newChild2 };
            
            //Act
            _population.AddChildren(newChildren);

            //Assert
            CollectionAssert.Contains(_population.Children, newChild1);
            CollectionAssert.Contains(_population.Children, newChild2);
        }

        [TestMethod]
        public void WhenRemoveChildrenThenChildrenDoesNotContainsRemovedChildren()
        {
            //Arrange
            List<Individual<string>> childrenToRemove = new List<Individual<string>> { _midChild, _bestChild };

            //Act
            _population.RemoveChildren(childrenToRemove);

            //Assert
            CollectionAssert.DoesNotContain(_population.Children, _midChild);
            CollectionAssert.DoesNotContain(_population.Children, _bestChild);
        }

        [TestMethod]
        public void BestChildTest()
        {
            //Arrange

            //Act
            Individual<string> bestChild = _population.BestChild;

            //Assert
            Assert.AreEqual(_bestChild, bestChild);
        }

        public void BestChildFitnessTest()
        {
            //Arrange

            //Act
            double bestChildFitness = _population.BestChildFitness;

            //Assert
            Assert.AreEqual(_bestChild.Fitness, bestChildFitness);
        }

        #endregion

        #region Merge

        [TestMethod]
        public void WhenAddChildrenToIndividualsThenIndividualsCountainsChildren()
        {
            //Arrange

            //Act
            _population.AddChildrenToParents();

            //Assert
            CollectionAssert.Contains(_population.Parents, _midChild);
            CollectionAssert.Contains(_population.Parents, _worstChild);
            CollectionAssert.Contains(_population.Parents, _bestChild);
        }

        [TestMethod]
        public void WhenClearChildrenThenChildrenColectionIsEmpty()
        {
            //Arrange

            //Act
            _population.ClearChildren();

            //Assert
            Assert.AreEqual(0, _population.Children.Count);
        }

        #endregion
    }
}
