﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace EvolutionFrameworkUnitTest.Printer
{
    [TestClass]
    public class ArrayPrinterTests
    {
        [TestMethod]
        public void WhenGetMessageIsCallesThenCorrestMessageIsReturned()
        {
            //Arrange
            string expectedMessage = "3: 1 2 3  - 1,234567867";
            ArrayPrinter<double> arrayPrinter = new ArrayPrinter<double>();
            Individual<double[]> individual = new Individual<double[]>(new[] {1.0, 2.0, 3.0})
                                                  {Fitness = 1.234567867};
            Population<double[]> population = new Population<double[]>(new [] {individual});
            //Act
            string message = arrayPrinter.GetMessage(population, 3);
            
            //Assert
            Assert.AreEqual(expectedMessage, message);
        }
    }
}


