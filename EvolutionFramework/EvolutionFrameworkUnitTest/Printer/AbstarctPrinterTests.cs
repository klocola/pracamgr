﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace EvolutionFrameworkUnitTest.Printer
{
    [TestClass]
    public class AbstarctPrinterTests
    {
        [TestMethod]
        public void WhenPrintIsCallesThenGetMessageIsCalled()
        {
            //Arrange
            Mock<AbstractPrinter<double[]>> abstarctPrinterMock = new Mock<AbstractPrinter<double[]>>();
            abstarctPrinterMock.Setup( p => p.GetMessage(It.IsAny<Population<double[]>>(), It.IsAny<int>())).Returns(It.IsAny<string>);
            Individual<double[]> individual = new Individual<double[]>(new[] {1.0, 2.0, 3.0})
                                                  {Fitness = 1.234567867};
            Population<double[]> population = new Population<double[]>(new [] {individual});
            //Act
            abstarctPrinterMock.Object.Print(population, 3);
            
            //Assert
            abstarctPrinterMock.Verify(p => p.GetMessage(It.IsAny<Population<double[]>>(), It.IsAny<int>()), Times.Once());
        }
    }
}

