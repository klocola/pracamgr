﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvolutionFrameworkUnitTest.TerminateCondition
{
    [TestClass]
    public class GenerationCountLimitationTests
    {
        [TestMethod]
        public void WhenGenerationCountLimitationIsExceededThenIsEvolutionOverReturnTrue()
        {
            //Arrange
            GenerationCountLimitationCondition<byte> generationCountLimitation = new GenerationCountLimitationCondition<byte>(3);
            Population<byte> population = new Population<byte>(new List<Individual<byte>>()) {GenerationNumber = 4};

            //Act
            bool isEvolutionOver = generationCountLimitation.IsEvolutionOver(population);

            //Assert
            Assert.IsTrue(isEvolutionOver);
        }

        [TestMethod]
        public void WhenGenerationCountLimitationIsNotExceededThenIsEvolutionOverReturnFalse()
        {
            //Arrange
            GenerationCountLimitationCondition<byte> generationCountLimitation = new GenerationCountLimitationCondition<byte>(6);
            Population<byte> population = new Population<byte>(new List<Individual<byte>>()) {GenerationNumber = 4};

            //Act
            bool isEvolutionOver = generationCountLimitation.IsEvolutionOver(population);

            //Assert
            Assert.IsFalse(isEvolutionOver);
        }
    }
}
