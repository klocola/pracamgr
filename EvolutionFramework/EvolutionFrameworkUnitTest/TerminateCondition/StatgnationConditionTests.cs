﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvolutionFrameworkUnitTest.TerminateCondition
{
    [TestClass]
    public class StatgnationConditionTests
    {
        [TestMethod]
        public void WhenThereIsNotStagnationThenIsEvolutionOverReturnFalse()
        {
            //Arrange
            StagnationCondition<byte> stagnationCondition = new StagnationCondition<byte>(1);
            Individual<byte> individual = new Individual<byte>(new byte()) {Fitness = 1};
            Population<byte> population = new Population<byte>(new List<Individual<byte>>
                                                                   {
                                                                       individual
                                                                   });

            //Act
            stagnationCondition.IsEvolutionOver(population);
            population.AddParent(new Individual<byte>(new byte()) {Fitness = 2});
            stagnationCondition.IsEvolutionOver(population);
            population.AddParent(new Individual<byte>(new byte()) { Fitness = 3 });
            bool isEvolutionOver = stagnationCondition.IsEvolutionOver(population);
            population.AddParent(new Individual<byte>(new byte()) { Fitness = 2.4 });

            //Assert
            Assert.IsFalse(isEvolutionOver);
        }

        [TestMethod]
        public void WhenThereIsStagnationThenIsEvolutionOverReturnTrue()
        {
            //Arrange
            StagnationCondition<byte> stagnationCondition = new StagnationCondition<byte>(2);
            Individual<byte> individual = new Individual<byte>(new byte()) {Fitness = 1};
            Population<byte> population = new Population<byte>(new List<Individual<byte>>
                                                                   {
                                                                       individual
                                                                   });

            //Act
            stagnationCondition.IsEvolutionOver(population);
            stagnationCondition.IsEvolutionOver(population);
            stagnationCondition.IsEvolutionOver(population);
            bool isEvolutionOver = stagnationCondition.IsEvolutionOver(population);

            //Assert
            Assert.IsTrue(isEvolutionOver);
        }

        [TestMethod]
        public void WhenThereIsStagnationAndMaxGenerationCountIsNotExceededThenIsEvolutionOverReturnFalse()
        {
            //Arrange
            StagnationCondition<byte> stagnationCondition = new StagnationCondition<byte>(5);
            Individual<byte> individual = new Individual<byte>(new byte()) { Fitness = 1 };
            Population<byte> population = new Population<byte>(new List<Individual<byte>>
                                                                   {
                                                                       individual
                                                                   });

            //Act
            stagnationCondition.IsEvolutionOver(population);
            stagnationCondition.IsEvolutionOver(population);
            bool isEvolutionOver = stagnationCondition.IsEvolutionOver(population);

            //Assert
            Assert.IsFalse(isEvolutionOver);
        }
    }
}
