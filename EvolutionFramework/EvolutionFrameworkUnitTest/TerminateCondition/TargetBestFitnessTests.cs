﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvolutionFrameworkUnitTest.TerminateCondition
{
    [TestClass]
    public class TargetBestFitnessTests
    {
        private Individual<string> _bestIndividual;
        private Individual<string> _worstIndividual;
        private Individual<string> _midIndividual;
        private Population<string> _population;

        [TestInitialize]
        public void TestInitialize()
        {
            _bestIndividual = new Individual<string>("_bestIndividual") { Fitness = 1 };
            _worstIndividual = new Individual<string>("_worstIndividual") { Fitness = 0.1 };
            _midIndividual = new Individual<string>("_midIndividual") { Fitness = 0.5 };
            List<Individual<string>> individuals = new List<Individual<string>> { _bestIndividual, _worstIndividual, _midIndividual };
            _population = new Population<string>(individuals);
        }

        [TestMethod]
        public void WhenBestFitnessIsSmallerThanTargetBestFitnessThenIsEvolutionOverReturnFalse()
        {
            //Arrange
            const int targetBestFitness = 10;
            TargetBestFitnessCondition<string> targetBestFitnessCondition = new TargetBestFitnessCondition<string>(targetBestFitness);

            //Act
            bool isEvolutionOver = targetBestFitnessCondition.IsEvolutionOver(_population);

            //Assert
            Assert.IsFalse(isEvolutionOver);
        }

        [TestMethod]
        public void WhenBestFitnessIsNotSmallerThanTargetBestFitnessThenIsEvolutionOverReturnFalse()
        {
            //Arrange
            const int targetBestFitness = 1;
            TargetBestFitnessCondition<string> targetBestFitnessCondition = new TargetBestFitnessCondition<string>(targetBestFitness);

            //Act
            bool isEvolutionOver = targetBestFitnessCondition.IsEvolutionOver(_population);

            //Assert
            Assert.IsTrue(isEvolutionOver);
        }
    }
}
