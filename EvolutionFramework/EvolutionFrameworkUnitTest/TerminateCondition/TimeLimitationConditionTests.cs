﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvolutionFrameworkUnitTest.TerminateCondition
{
    [TestClass]
    public class TimeLimitationConditionTests
    {
        [TestMethod]
        public void WhenTimeLimitationIsExceededThenIsEvolutionOverReturnTrue()
        {
            //Arrange
            TimeLimitationCondition<byte> timeLimitationCondition = new TimeLimitationCondition<byte>(new TimeSpan(0,0,0,0));
            Population<byte> population = new Population<byte>(new List<Individual<byte>>());

            //Act
            bool isEvolutionOver = timeLimitationCondition.IsEvolutionOver(population);

            //Assert
            Assert.IsTrue(isEvolutionOver);
        }

        [TestMethod]
        public void WhenTimeLimitationIsNotExceededThenIsEvolutionOverReturnFalse()
        {
            //Arrange
            TimeLimitationCondition<byte> timeLimitationCondition = new TimeLimitationCondition<byte>(new TimeSpan(1, 0, 0));
            Population<byte> population = new Population<byte>(new List<Individual<byte>>());

            //Act
            bool isEvolutionOver = timeLimitationCondition.IsEvolutionOver(population);

            //Assert
            Assert.IsFalse(isEvolutionOver);
        }
    }
}
