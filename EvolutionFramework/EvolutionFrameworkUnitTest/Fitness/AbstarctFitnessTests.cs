﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace EvolutionFrameworkUnitTest.Fitness
{
    [TestClass]
    public class AbstarctFitnessTests
    {
        private Population<double[]> _population;

        [TestInitialize]
        public void TestInitialize()
        {
            List<Individual<double[]>> individuals = new List<Individual<double[]>>
                                                         {
                                                             new Individual<double[]>(null),
                                                             new Individual<double[]>(null),
                                                             new Individual<double[]>(null)
                                                         };
            List<Individual<double[]>> children = new List<Individual<double[]>>
                                                         {
                                                             new Individual<double[]>(null),
                                                             new Individual<double[]>(null)
                                                         };

            _population = new Population<double[]>(individuals);
            _population.AddChildren(children);
        }

        [TestMethod]
        public void WhenUpdateIndividualsFitnessIsCalledThenCalculateFitnessIsCalledCorrectTimes()
        {
            //Arrange
            Mock<AbstractFitness<double[]>> mock = new Mock<AbstractFitness<double[]>>();
            mock.Setup(f => f.CalculateFitness(It.IsAny<Population<double[]>>(), It.IsAny<Individual<double[]>>()))
                .Returns(It.IsAny<double>());

            //Act
            mock.Object.UpdateParentsFitness(_population);
            
            //Assert
            //Assert
            mock.Verify(f => f.CalculateFitness(It.IsAny<Population<double[]>>(), It.IsAny<Individual<double[]>>()), Times.Exactly(3));
        }

        [TestMethod]
        public void WhenUpdateChildrenFitnessIsCalledThenCalculateFitnessIsCalledCorrectTimes()
        {
            //Arrange
            Mock<AbstractFitness<double[]>> mock = new Mock<AbstractFitness<double[]>>();
            mock.Setup(f => f.CalculateFitness(It.IsAny<Population<double[]>>(), It.IsAny<Individual<double[]>>()))
                .Returns(It.IsAny<double>());

            //Act
            mock.Object.UpdateChildrenFitness(_population);

            //Assert
            mock.Verify(f => f.CalculateFitness(It.IsAny<Population<double[]>>(), It.IsAny<Individual<double[]>>()), Times.Exactly(2));
        }
    }
}
