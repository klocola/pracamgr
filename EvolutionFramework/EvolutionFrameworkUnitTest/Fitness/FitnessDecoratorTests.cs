﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace EvolutionFrameworkUnitTest.Fitness
{
    [TestClass]
    public class FitnessDecoratorTests
    {
        private Population<double[]> _population;

        [TestInitialize]
        public void TestInitialize()
        {
            List<Individual<double[]>> individuals = new List<Individual<double[]>>
                                                         {
                                                             new Individual<double[]>(null),
                                                             new Individual<double[]>(null),
                                                             new Individual<double[]>(null)
                                                         };
            List<Individual<double[]>> children = new List<Individual<double[]>>
                                                         {
                                                             new Individual<double[]>(null),
                                                             new Individual<double[]>(null)
                                                         };

            _population = new Population<double[]>(individuals);
            _population.AddChildren(children);
        }

        [TestMethod]
        public void WhenUpdateIndividualsFitnessIsCalledThenCalculateFitnessIsCalledCorrectTimes()
        {
            //Arrange
            Mock<IFitness<double[]>> fitnessMock = new Mock<IFitness<double[]>>();

            Mock<AbstractFitnessDecorator<double[]>> mock = new Mock<AbstractFitnessDecorator<double[]>>(fitnessMock.Object);
            mock.Protected().Setup("AdaptFitness", ItExpr.IsAny<Population<double[]>>());

            //Act
            mock.Object.UpdateParentsFitness(_population);

            //Assert
            mock.Protected().Verify("AdaptFitness", Times.Exactly(1), ItExpr.IsAny<Population<double[]>>());
        }

        [TestMethod]
        public void WhenUpdateChildrenFitnessIsCalledThenCalculateFitnessIsCalledCorrectTimes()
        {
            //Arrange
            Mock<IFitness<double[]>> fitnessMock = new Mock<IFitness<double[]>>();

            Mock<AbstractFitnessDecorator<double[]>> mock = new Mock<AbstractFitnessDecorator<double[]>>(fitnessMock.Object);
            mock.Protected().Setup("AdaptFitness", ItExpr.IsAny<Population<double[]>>());

            //Act
            mock.Object.UpdateChildrenFitness(_population);

            //Assert
            mock.Protected().Verify("AdaptFitness", Times.Exactly(1), ItExpr.IsAny<Population<double[]>>());
        }
    }
}
