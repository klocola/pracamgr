﻿using System;
using System.ComponentModel;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvolutionFramework;
using Moq;

namespace EvolutionFrameworkUnitTest.Engine
{
    [TestClass]
    public class EngineTests
    {
        private Mock<IFitness<double[]>> _fintessMock;
        private Mock<ITerminationCondition<double[]>> _terminateConditionMock;
        private List<IEvolutionTool<double[]>> _evolutionTools;
        private Mock<IIndividualsFactory<double[]>> _individualsFactoryMock;
        private Mock<AbstractPrinter<double[]>> _printerMock;
        private Mock<IEvolutionTool<double[]>> _evolutionToolMock;
        
        [TestInitialize]
        public void TestInitialize()
        {
            _fintessMock = new Mock<IFitness<double[]>>();
            _terminateConditionMock = new Mock<ITerminationCondition<double[]>>();
            _individualsFactoryMock = new Mock<IIndividualsFactory<double[]>>();
            _evolutionTools = new List<IEvolutionTool<double[]>>();
            _printerMock = new Mock<AbstractPrinter<double[]>>();
            _evolutionToolMock = new Mock<IEvolutionTool<double[]>>();
        }

        #region IncorrectConfigurationException

        [TestMethod]
        [ExpectedException(typeof(IncorrectConfigurationException))]
        public void WhenFintessIsNullThenIncorrectConfigurationException()
        {
            //Arrange
            TestConfiguration<double[]> testConfiguration =
                new TestConfiguration<double[]>(false,
                    1,
                    null,
                    null,
                    _terminateConditionMock.Object,
                    _evolutionTools,
                    _individualsFactoryMock.Object);
            Engine<double[]> engine = new Engine<double[]>(testConfiguration);

            //Act
            engine.RunEvolution();

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(IncorrectConfigurationException))]
        public void WhenTerminateConditionIsNullThenIncorrectConfigurationException()
        {
            //Arrange
            TestConfiguration<double[]> testConfiguration =
                new TestConfiguration<double[]>(false,
                    1,
                    _fintessMock.Object,
                    null,
                    null,
                    _evolutionTools,
                    _individualsFactoryMock.Object);
            Engine<double[]> engine = new Engine<double[]>(testConfiguration);

            //Act
            engine.RunEvolution();

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(IncorrectConfigurationException))]
        public void WhenEvolutionToolsIsNullThenIncorrectConfigurationException()
        {
            //Arrange
            TestConfiguration<double[]> testConfiguration =
                new TestConfiguration<double[]>(false,
                    1,
                    _fintessMock.Object,
                    null,
                    _terminateConditionMock.Object,
                    null,
                    _individualsFactoryMock.Object);
            Engine<double[]> engine = new Engine<double[]>(testConfiguration);

            //Act
            engine.RunEvolution();

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(IncorrectConfigurationException))]
        public void WhenIndividualsFactoryIsNullThenIncorrectConfigurationException()
        {
            //Arrange
            TestConfiguration<double[]> testConfiguration =
                new TestConfiguration<double[]>(false,
                    1,
                    _fintessMock.Object,
                    null,
                    _terminateConditionMock.Object,
                    _evolutionTools,
                    null);
            Engine<double[]> engine = new Engine<double[]>(testConfiguration);

            //Act
            engine.RunEvolution();

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(IncorrectConfigurationException))]
        public void WhenPopulationCountIsZeroThenIncorrectConfigurationException()
        {
            //Arrange
            TestConfiguration<double[]> testConfiguration =
                new TestConfiguration<double[]>(false,
                    0,
                    _fintessMock.Object,
                    null,
                    _terminateConditionMock.Object,
                    _evolutionTools,
                    _individualsFactoryMock.Object);
            Engine<double[]> engine = new Engine<double[]>(testConfiguration);

            //Act
            engine.RunEvolution();

            //Assert
        }

        #endregion
        
        [TestMethod]
        public void WhenRunEvolutionCallsThenCorrectCountOfCallingEvoltionToolsOperation()
        {
            //Arrange
            _evolutionToolMock.Setup(e => e.Apply(It.IsAny<Population<double[]>>()));
            _evolutionTools.Add(_evolutionToolMock.Object);
            _terminateConditionMock.Setup(
                t => t.IsEvolutionOver(It.Is<Population<double[]>>(p => p.GenerationNumber == 0))).Returns(false);
            _terminateConditionMock.Setup(
                t => t.IsEvolutionOver(It.Is<Population<double[]>>(p => p.GenerationNumber == 1))).Returns(true);
            _individualsFactoryMock.Setup(i => i.GenerateIndividuals(It.IsAny<int>())).Returns(
                new List<Individual<double[]>>
                     {
                         new Individual<double[]>(new double[0]) {Fitness = 1}
                     });
            TestConfiguration<double[]> testConfiguration =
                new TestConfiguration<double[]>(false,
                    1,
                    _fintessMock.Object,
                    _printerMock.Object,
                    _terminateConditionMock.Object,
                    _evolutionTools,
                    _individualsFactoryMock.Object);
            Engine<double[]> engine = new Engine<double[]>(testConfiguration);


            //Act
            engine.RunEvolution();
            
            //Assert
            _evolutionToolMock.Verify(e => e.Apply(It.IsAny<Population<double[]>>()), Times.Exactly(1));
        }

        #region Run evolution


        #endregion

        #region TestConfiguration

        private class TestConfiguration<T> : IConfiguration<T>
        {
            private readonly bool _isFitnessVariable;
            private readonly int _populationSize;
            private readonly IFitness<T> _fitness;
            private readonly AbstractPrinter<T> _printer;
            private readonly ITerminationCondition<T> _terminateCondition;
            private readonly IEnumerable<IEvolutionTool<T>> _evolutionTools;
            private readonly IIndividualsFactory<T> _individualsFactory;
            
            public TestConfiguration(bool isFitnessVariable,
                int populationSize,
                IFitness<T> fitness,
                AbstractPrinter<T> printer,
                ITerminationCondition<T> terminateCondition,
                IEnumerable<IEvolutionTool<T>> evolutionTools,
                IIndividualsFactory<T> individualsFactory)
            {
                _isFitnessVariable = isFitnessVariable;
                _populationSize = populationSize;
                _fitness = fitness;
                _printer = printer;
                _terminateCondition = terminateCondition;
                _evolutionTools = evolutionTools;
                _individualsFactory = individualsFactory;
            }

            public bool IsFitnessVariable
            {
                get { return _isFitnessVariable; }
            }

            public int PopulationSize
            {
                get { return _populationSize; }
            }

            public IFitness<T> Fitness
            {
                get { return _fitness; }
            }

            public AbstractPrinter<T> Printer
            {
                get { return _printer; }
            }

            public ITerminationCondition<T> TerminateCondition
            {
                get { return _terminateCondition; }
            }

            public IEnumerable<IEvolutionTool<T>> EvolutionTools
            {
                get { return _evolutionTools;}
            }

            public IIndividualsFactory<T> IndividualsFactory
            {
                get { return _individualsFactory; }
            }
        }

        #endregion
    }
}
