﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Extensions;

namespace EvolutionFrameworkUnitTest.IndividualsFactory
{
    [TestClass]
    public class DoubleArrayIndividualFacotoryTests
    {
        [TestMethod]
        public void CreateDoubleArrayIndividualTest()
        {
            //Arrange
            TestRandom random = new TestRandom(new[] { 0.3, 0.44, 0.5, 0});
            DoubleArrayIndividualFactory doubleArrayIndividual =
                new DoubleArrayIndividualFactory(random, 4, 2);
            double[] expectedChromosome = new[] { -0.8, -0.24, 0, -2};

            //Act
            Individual<double[]> individual = doubleArrayIndividual.GenerateIndividual();

            //Assert
            for (int index = 0; index < expectedChromosome.Length; index++)
            {
                Assert.IsTrue(expectedChromosome[index].EqualsWeakPrecision(individual.Chromosome[index]));
            }
        }
    }
}
