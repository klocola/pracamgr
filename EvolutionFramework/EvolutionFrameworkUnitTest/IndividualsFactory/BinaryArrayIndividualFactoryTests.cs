﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvolutionFramework;

namespace EvolutionFrameworkUnitTest
{

    [TestClass]
    public class BinaryArrayIndividualFactoryTests
    {
        private const int ChormosomeSize = 60;

        [TestMethod]
        public void CreateBinaryIndividualTest()
        {
            //Arrange
            TestRandom random = new TestRandom(0.6, 6);
            BinaryArrayIndividualFactory binaryIndividualFactory = new BinaryArrayIndividualFactory(random, ChormosomeSize);
            int[] expectedChromosome = new int[ChormosomeSize];
            for (int index = 0; index < expectedChromosome.Length; index++)
            {
                expectedChromosome[index] = 1;
            }

            //Act
            Individual<int[]> individual = binaryIndividualFactory.GenerateIndividual();

            //Assert
            CollectionAssert.AreEqual(expectedChromosome, individual.Chromosome);
        }

        [TestMethod]
        public void CreateBinaryIndividualsCountTest()
        {
            //Arrange
            const int individualsCount = 20;
            TestRandom random = new TestRandom(0.3, 6);
            BinaryArrayIndividualFactory binaryIndividualFactory = new BinaryArrayIndividualFactory(random, ChormosomeSize);
            
            //Act
            IEnumerable<Individual<int[]>> individual = binaryIndividualFactory.GenerateIndividuals(individualsCount);

            //Assert
            Assert.AreEqual(individualsCount, individual.Count());
        }
    }
}
