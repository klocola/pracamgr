﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EvolutionFramework;

namespace EvolutionFrameworkUnitTest.IndividualsFactory
{
    [TestClass]
    public class AbstractRandomIndividualsFactoryTests
    {
        [TestMethod]
        public void WhenGenerateIndividualsIsCalledThenGenerateIndividualIsCalledCorrectTimes()
        {
            //Arrange
            const int individualsCount = 10;
            TestRandom random = new TestRandom(0.5, 1);
            Mock<AbstractRandomIndividualsFactory<int[]>> mock = new Mock<AbstractRandomIndividualsFactory<int[]>>(random);
            mock.Setup(mutation => mutation.GenerateIndividual())
                .Returns(It.IsAny<Individual<int[]>>());
            mock.CallBase = true;

            //Act
            IEnumerable<Individual<int[]>> idividuals = mock.Object.GenerateIndividuals(individualsCount);
            foreach (Individual<int[]> individual in idividuals)
            {
            }

            //Assert
            mock.Verify(abstractIndividualsFactory => abstractIndividualsFactory.GenerateIndividual(), Times.Exactly(individualsCount));
        }
    }
}
