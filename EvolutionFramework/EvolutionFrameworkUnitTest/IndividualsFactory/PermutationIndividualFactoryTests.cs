﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvolutionFrameworkUnitTest.IndividualsFactory
{
    [TestClass]
    public class PermutationIndividualFactoryTests
    {
        [TestMethod]
        public void CreatePermutationIndividualTest()
        {
            //Arrange
            TestRandom random = new TestRandom(new[] {0, 4, 6, 2, 8, 7, 1, 3, 5, 9});
            PermutationIndividualFactory<int> permutationIndividualFactory =
                new PermutationIndividualFactory<int>(random, new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            int[] expectedChromosome = new[] { 1, 5, 7, 3, 9, 8, 2, 4, 6, 10 };

            //Act
            Individual<int[]> individual = permutationIndividualFactory.GenerateIndividual();

            //Assert
            CollectionAssert.AreEqual(expectedChromosome, individual.Chromosome);
        }
    }
}
