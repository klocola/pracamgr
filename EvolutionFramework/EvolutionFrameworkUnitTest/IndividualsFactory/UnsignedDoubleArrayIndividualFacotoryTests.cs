﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Extensions;

namespace EvolutionFrameworkUnitTest.IndividualsFactory
{
    [TestClass]
    public class UnsignedDoubleArrayIndividualFacotoryTests
    {
        [TestMethod]
        public void CreateUnsignedDoubleArrayIndividualTest()
        {
            //Arrange
            TestRandom random = new TestRandom(new[] { 0.3, 0.44, 0.56, 0.7, 0.18, 0.7, 0.41, 0.3, 0.5, 0.9 });
            UnsignedDoubleArrayIndividualFactory unsignedDoubleArrayIndividual =
                new UnsignedDoubleArrayIndividualFactory(random, 10, 1);
            double[] expectedChromosome = new[] { 0.3, 0.44, 0.56, 0.7, 0.18, 0.7, 0.41, 0.3, 0.5, 0.9 };

            //Act
            Individual<double[]> individual = unsignedDoubleArrayIndividual.GenerateIndividual();

            //Assert
            for (int index = 0; index < expectedChromosome.Length; index++)
            {
                Assert.IsTrue(expectedChromosome[index].EqualsWeakPrecision(individual.Chromosome[index]));
            }
        }
    }
}
