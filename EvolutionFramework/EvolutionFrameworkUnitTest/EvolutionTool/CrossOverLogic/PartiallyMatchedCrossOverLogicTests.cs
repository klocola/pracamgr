﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvolutionFramework;

namespace EvolutionFrameworkUnitTest.EvolutionTool.CrossOver
{
    [TestClass]
    public class PartiallyMatchedCrossOverLogicTests
    {
        private List<int[]> _chromosomes;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosomes = new List<int[]>
                               {
                                    new[] { 6, 1, 4, 8, 2, 9, 0, 7, 3, 5 },
                                    new[] { 7, 2, 9, 3, 6, 1, 5, 4, 8, 0 }
                               };
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectChildren()
        {
            //Arrange
            int[][] expectedchildrens = new[]
                                            {
                                                    new[] {2, 9, 4, 3, 6, 1, 0, 7, 8, 5},
                                                    new[] {7, 6, 1, 8, 2, 9, 5, 4, 3, 0}
                                                };
            TestRandom random = new TestRandom(new[] {0.5}, new[] {3, 6});
            PartiallyMatchedCrossOverLogic<int> partiallyMatchedCrossOver = new PartiallyMatchedCrossOverLogic<int>(random);

            //Act
            int[][] children = partiallyMatchedCrossOver.CrossOver(_chromosomes).ToArray();

            //Assert
            CollectionAssert.AreEqual(expectedchildrens[0], children[0]);
            CollectionAssert.AreEqual(expectedchildrens[1], children[1]);
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectCountOfChildren()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5);
            PartiallyMatchedCrossOverLogic<int> partiallyMatchedCrossOver = new PartiallyMatchedCrossOverLogic<int>(random);

            //Act
            int[][] children = partiallyMatchedCrossOver.CrossOver(_chromosomes).ToArray();

            //Assert
            Assert.AreEqual(2, children.Count());
        }
    }
}
