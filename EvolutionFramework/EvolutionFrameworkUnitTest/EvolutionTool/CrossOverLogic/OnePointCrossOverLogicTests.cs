﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvolutionFramework;

namespace EvolutionFrameworkUnitTest.EvolutionTool.CrossOver
{
    [TestClass]
    public class OnePointCrossOverLogicTests
    {
        private List<int[]> _chromosomes;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosomes = new List<int[]>
                               {
                                   new[] { 1, 1, 1, 1, 1, 1 },
                                   new[] { 0, 0, 0, 0, 0, 0 }
                               };
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectChildren()
        {
            //Arrange
            int[][] expectedchildrens = new[]
                                            {
                                                    new[] {1, 1, 1, 0, 0, 0},
                                                    new[] {0, 0, 0, 1, 1, 1}
                                                };
            TestRandom random = new TestRandom(0.5, 3);
            OnePointCrossOverLogic<int> onePointCrossOver = new OnePointCrossOverLogic<int>(random);

            //Act
            int[][] children = onePointCrossOver.CrossOver(_chromosomes).ToArray();

            //Assert
            CollectionAssert.AreEqual(expectedchildrens[0], children[0]);
            CollectionAssert.AreEqual(expectedchildrens[1], children[1]);
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectCountOfChildren()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5);
            OnePointCrossOverLogic<int> binaryChangeMutation = new OnePointCrossOverLogic<int>(random);

            //Act
            ICollection<int[]> children = binaryChangeMutation.CrossOver(_chromosomes);

            //Assert
            Assert.AreEqual(2, children.Count);
        }
    }
}
