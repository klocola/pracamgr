﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Extensions;

namespace EvolutionFrameworkUnitTest.EvolutionTool.CrossOver
{
    [TestClass]
    public class LineCrossOverLogicTests
    {
        private List<double[]> _chromosomes;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosomes = new List<double[]>
                                             {
                                                 new[] {0.5, 0.4, 0.34, 0.345, 1.34, 6.78},
                                                 new[] {0.56, 1.46, 4.54, 1.456, 1.34, 0.86}
                                             };
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectChildren()
        {
            double[] expectedchildren = new[] {0.5525, 1.3275, 4.0150000000000006, 1.317125, 1.34, 1.6};
            TestRandom random = new TestRandom(0.3);
            LineCrossOverLogic lineCrossOver = new LineCrossOverLogic(random);

            //Act
            double[][] children = lineCrossOver.CrossOver(_chromosomes).ToArray();

            //Assert
            for (int index = 0; index < expectedchildren.Length; index++)
            {
                Assert.IsTrue(expectedchildren[index].EqualsWeakPrecision(children[0][index]));
            }
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectCountOfChildren()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5);
            LineCrossOverLogic lineCrossOver = new LineCrossOverLogic(random);

            //Act
            ICollection<double[]> children = lineCrossOver.CrossOver(_chromosomes);

            //Assert
            Assert.AreEqual(1, children.Count);
        }
    }
}
