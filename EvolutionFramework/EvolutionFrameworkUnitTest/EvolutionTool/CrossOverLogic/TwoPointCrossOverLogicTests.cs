﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvolutionFrameworkUnitTest.EvolutionTool.CrossOver
{
    [TestClass]
    public class TwoPointCrossOverLogicTests
    {
        private List<int[]> _chromosomes;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosomes = new List<int[]>
                               {
                                   new[] { 1, 1, 1, 1, 1, 1 },
                                   new[] { 0, 0, 0, 0, 0, 0 }
                               };
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectChildren()
        {
            //Arrange
            int[][] expectedchildrens = new[]
                                            {
                                                    new[] {1, 0, 1, 1, 1, 1},
                                                    new[] {0, 1, 0, 0, 0, 0}
                                                };
            TestRandom random = new TestRandom(0.5);
            TwoPointCrossOverLogic<int> binaryChangeMutation = new TwoPointCrossOverLogic<int>(random);

            //Act
            int[][] children = binaryChangeMutation.CrossOver(_chromosomes).ToArray();

            //Assert
            CollectionAssert.AreEqual(expectedchildrens[0], children[0]);
            CollectionAssert.AreEqual(expectedchildrens[1], children[1]);
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectCountOfChildren()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5);
            TwoPointCrossOverLogic<int> binaryChangeMutation = new TwoPointCrossOverLogic<int>(random);

            //Act
            int[][] children = binaryChangeMutation.CrossOver(_chromosomes).ToArray();

            //Assert
            Assert.AreEqual(2, children.Count());
        }
    }
}
