﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Extensions;

namespace EvolutionFrameworkUnitTest.EvolutionTool.CrossOver
{
    [TestClass]
    public class IntermediateCrossOverLogicTests
    {
        private ICollection<double[]> _chromosomes;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosomes = new List<double[]>
                            {
                                new[] { 0.5, 0.4, 0.34, 0.345, 1.34, 6.78 },
                                new[] { 0.56, 1.46, 4.54, 1.456, 1.34, 0.86 }
                            };
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectChildren()
        {
            double[] expectedchild = new[] { 0.5525, 0.665, 4.015, 0.62275, 1.34, 5.3};
            TestRandom random = new TestRandom();
            IntermediateCrossOverLogic intermediateCrossOver = new IntermediateCrossOverLogic(random);

            //Act
            double[] child = intermediateCrossOver.CrossOver(_chromosomes).First();
            for (int index = 0; index < child.Length; index++)
            {
                child[index] = Math.Round(child[index], 5);
            }

            //Assert
            for (int index = 0; index < expectedchild.Length; index++)
            {
                Assert.IsTrue(expectedchild[index].EqualsWeakPrecision(child[index]));
            }
        }

        [TestMethod]
        public void WhenCrossOverProbabilityIsOneThenCorrectCountOfChildren()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5);
            IntermediateCrossOverLogic intermediateCrossOver = new IntermediateCrossOverLogic(random);

            //Act
            ICollection<double[]> children = intermediateCrossOver.CrossOver(_chromosomes);

            //Assert
            Assert.AreEqual(1, children.Count);
        }
    }
}

