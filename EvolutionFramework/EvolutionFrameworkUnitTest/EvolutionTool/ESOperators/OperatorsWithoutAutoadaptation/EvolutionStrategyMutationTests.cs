﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace EvolutionFrameworkUnitTest.EvolutionTool.ESOperators
{
    [TestClass]
    public class EvolutionStrategyMutationTests
    {
        private const int IndividualsCount = 10;
        private Population<int[]> _population;
        private List<Individual<int[]>> _individuals;

        [TestInitialize]
        public void TestInitialize()
        {
            _individuals = new List<Individual<int[]>>();
            for (int index = 0; index < IndividualsCount; index++)
            {
                _individuals.Add(new Individual<int[]>(new int[0]));
            }

            _population = new Population<int[]>(_individuals);
        }

        [TestMethod]
        public void WhenApplayIsCalledThenCorrectCountOfChildren()
        {
            //Arrange
            const int childrenCount = 3;
            TestRandom random = new TestRandom(0.5, 1);
            Mock<IMutationLogic<int[]>> mutationLogic = new Mock<IMutationLogic<int[]>>();
            mutationLogic.Setup(m => m.Mutate(It.IsAny<int[]>())).Returns(It.IsAny<int[]>());
            EvolutionStrategyMutation<int[]> evolutionStrategyOperator = new EvolutionStrategyMutation<int[]>(random, mutationLogic.Object, childrenCount);

            //Act
            evolutionStrategyOperator.Apply(_population);

            //Assert
            Assert.AreEqual(childrenCount, _population.Children.Count);
        }
    }
}
