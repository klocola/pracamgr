﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace EvolutionFrameworkUnitTest.EvolutionTool.ESOperators.OperatorsWithAutoAdaptation
{
    [TestClass]
    public class AbstractEvolutionStrategyOperatorWithAutoAdaptationTests
    {
        private Mock<AbstractEvolutionStrategyOperatorWithAutoAdaptation<int[]>> _abstractEvolutionStrategyOperatorWithAutoAdaptationMock;

        [TestInitialize]
        public void TestInitialize()
        {
            Mock<IMutationLogicWithRange<int[]>> mutationLogicWithRangeMock = new Mock<IMutationLogicWithRange<int[]>>(); 
            Mock<AbstractFitness<int[]>> abstractFitness = new Mock<AbstractFitness<int[]>>();
            _abstractEvolutionStrategyOperatorWithAutoAdaptationMock =
                new Mock<AbstractEvolutionStrategyOperatorWithAutoAdaptation<int[]>>(abstractFitness.Object, mutationLogicWithRangeMock.Object);
        }

        [TestMethod]
        public void DecreasingRangeScaleSmallerThan1()
        {
            //Arrange

            //Act
            _abstractEvolutionStrategyOperatorWithAutoAdaptationMock.Object.DecreasingRangeScale = 1.7;

            //Assert
            Assert.AreEqual(0.99, _abstractEvolutionStrategyOperatorWithAutoAdaptationMock.Object.DecreasingRangeScale);
        }

        [TestMethod]
        public void IncreasingRangeScaleBiggerThan1()
        {
            //Arrange
            
            //Act
            _abstractEvolutionStrategyOperatorWithAutoAdaptationMock.Object.IncreasigRangeScale = 0.7;

            //Assert
            Assert.AreEqual(1.01, _abstractEvolutionStrategyOperatorWithAutoAdaptationMock.Object.IncreasigRangeScale);
        }
    }
}
