﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace EvolutionFrameworkUnitTest.EvolutionTool.ESOperators.OperatorsWithAutoAdaptation
{
    [TestClass]
    public class EvolutionStrategyMutationWithAutoAdaptationTests
    {
        private const int IndividualsCount = 10;
        private Population<int[]> _population;
        private List<Individual<int[]>> _individuals;

        [TestInitialize]
        public void TestInitialize()
        {
            _individuals = new List<Individual<int[]>>();
            for (int index = 0; index < IndividualsCount; index++)
            {
                _individuals.Add(new Individual<int[]>(new int[0]));
            }

            _population = new Population<int[]>(_individuals);
        }

        [TestMethod]
        public void WhenApplayIsCalledThenCorrectCountOfChildren()
        {
            //Arrange
            const int childrenCount = 3;
            TestRandom random = new TestRandom(0.5, 1);
            Mock<IMutationLogicWithRange<int[]>> mutationLogicMock = new Mock<IMutationLogicWithRange<int[]>>();
            Mock<AbstractFitness<int[]>> fitnessMock = new Mock<AbstractFitness<int[]>>();
            mutationLogicMock.Setup(m => m.Mutate(It.IsAny<int[]>())).Returns(It.IsAny<int[]>());
            EvolutionStrategyMutationWithAutoAdaptation<int[]> evolutionStrategyOperator = new EvolutionStrategyMutationWithAutoAdaptation<int[]>(random, fitnessMock.Object, childrenCount, mutationLogicMock.Object);

            //Act
            evolutionStrategyOperator.Apply(_population);

            //Assert
            Assert.AreEqual(childrenCount, _population.Children.Count);
        }

        [TestMethod]
        public void WhenApplyIsCalledThenCorrectCountOfCallOfTryAdaptMutationRange()
        {
            //Arrange
            const int childrenCount = 3;
            TestRandom random = new TestRandom(0.5, 1);
            Mock<IMutationLogicWithRange<int[]>> mutationLogicMock = new Mock<IMutationLogicWithRange<int[]>>();
            Mock<AbstractFitness<int[]>> fitnessMock = new Mock<AbstractFitness<int[]>>();
            mutationLogicMock.Setup(m => m.Mutate(It.IsAny<int[]>())).Returns(It.IsAny<int[]>());
            Mock<EvolutionStrategyMutationWithAutoAdaptation<int[]>> evolutionStrategyOperatorMock = 
                new Mock<EvolutionStrategyMutationWithAutoAdaptation<int[]>>(random, fitnessMock.Object, childrenCount, mutationLogicMock.Object);
            evolutionStrategyOperatorMock.CallBase = true;
            //Act
            evolutionStrategyOperatorMock.Object.Apply(_population);

            //Assert
            evolutionStrategyOperatorMock.Protected().Verify("TryAdaptMutationRange", Times.Exactly(1));
        }

        [TestMethod]
        public void WhenApplyIsCalledThenCorrectCountOfCallOfUpdateStatistic()
        {
            //Arrange
            const int childrenCount = 3;
            TestRandom random = new TestRandom(0.5, 1);
            Mock<IMutationLogicWithRange<int[]>> mutationLogicMock = new Mock<IMutationLogicWithRange<int[]>>();
            Mock<AbstractFitness<int[]>> fitnessMock = new Mock<AbstractFitness<int[]>>();
            mutationLogicMock.Setup(m => m.Mutate(It.IsAny<int[]>())).Returns(It.IsAny<int[]>());
            Mock<EvolutionStrategyMutationWithAutoAdaptation<int[]>> evolutionStrategyOperatorMock =
                new Mock<EvolutionStrategyMutationWithAutoAdaptation<int[]>>(random, fitnessMock.Object, childrenCount, mutationLogicMock.Object);
            evolutionStrategyOperatorMock.CallBase = true;
            //Act
            evolutionStrategyOperatorMock.Object.Apply(_population);

            //Assert
            evolutionStrategyOperatorMock.Protected().Verify("UpdateStatistic", Times.Exactly(childrenCount), ItExpr.IsAny<Population<int[]>>(), ItExpr.IsAny<Individual<int[]>>(), ItExpr.IsAny<Individual<int[]>>());
        }
    }
}
