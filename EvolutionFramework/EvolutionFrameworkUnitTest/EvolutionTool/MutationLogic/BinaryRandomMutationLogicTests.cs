﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvolutionFrameworkUnitTest.EvolutionTool.Mutation
{
    [TestClass]
    public class BinaryRandomMutationLogicTests
    {
        private int[] _chromosome;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosome = new[] { 1, 0, 1, 0, 1, 1 };
        }

        [TestMethod]
        public void WhenMutateProbabilityOneThenCorrectMutation()
        {
            //Arrange
            int[] expectedmutatedChromosome = new[] { 1, 1, 1, 1, 1, 1 };
            TestRandom random = new TestRandom(0.6, 1);
            BinaryRandomMutationLogic binaryRandomMutation = new BinaryRandomMutationLogic(random, 1);
            
            //Act
            int[] mutatedChromosome = binaryRandomMutation.Mutate(_chromosome);

            //Assert
            CollectionAssert.AreEqual(expectedmutatedChromosome, mutatedChromosome);
        }
    }
}
