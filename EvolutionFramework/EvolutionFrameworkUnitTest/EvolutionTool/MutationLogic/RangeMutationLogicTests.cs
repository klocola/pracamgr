﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Extensions;

namespace EvolutionFrameworkUnitTest.EvolutionTool.Mutation
{
    [TestClass]
    public class RangeMutationLogicTests
    {
        private double[] _chromosome;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosome = new[] { 1.2, 1.1, 1.56, 1.2, 0.4};
        }

        [TestMethod]
        public void WhenMutateProbabilityOneThenCorrectMutation()
        {
            //Arrange
            double[] expectedmutatedChromosome = new[] { 1.6, 1.5, 1.96, 1.6, 0.8};
            TestRandom random = new TestRandom(0.6);
            RangeMutationLogic rangeMutation = new RangeMutationLogic(random, 1, 2);

            //Act
            double[] mutatedChromosome = rangeMutation.Mutate(_chromosome);

            //Assert
            for (int index = 0; index < expectedmutatedChromosome.Length; index++)
            {
                Assert.IsTrue(expectedmutatedChromosome[index].EqualsWeakPrecision(mutatedChromosome[index]));   
            }
        }
    }
}
