﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvolutionFramework;

namespace EvolutionFrameworkUnitTest
{
    [TestClass]
    public class BinaryChangeMutationLogicTests
    {
        private int[] _chromosome;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosome = new[] { 1, 0, 1, 0, 1, 1 };
        }

        [TestMethod]
        public void WhenMutateProbabilityOneThenCorrectMutation()
        {
            //Arrange
            int[] expectedmutatedChromosome = new[] { 0, 1, 0, 1, 0, 0 };
            TestRandom random = new TestRandom(0.5, 1);
            BinaryChangeMutationLogic binaryChangeMutation = new BinaryChangeMutationLogic(random, 1);
            
            //Act
            int[] mutatedChromosome = binaryChangeMutation.Mutate(_chromosome);

            //Assert
            CollectionAssert.AreEqual(expectedmutatedChromosome, mutatedChromosome);
        }
    }
}
