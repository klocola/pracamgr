﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvolutionFramework;

namespace EvolutionFrameworkUnitTest.EvolutionTool.Mutation
{
    [TestClass]
    public class InversionMutationLogicTests
    {
        private string[] _chromosome;

        [TestInitialize]
        public void TestInitialize()
        {
            _chromosome = new[] { "a", "ab", "cd", "Z" };
        }

        [TestMethod]
        public void WhenMutateProbabilityOneThenCorrectMutation()
        {
            //Arrange
            string[] expectedmutatedChromosome = new[] { "a", "Z", "cd", "ab" };
            TestRandom random = new TestRandom(new[] {1, 3});
            InversionMutationLogic<string> inversionMutation = new InversionMutationLogic<string>(random);

            //Act
            string[] mutatedChromosome =  inversionMutation.Mutate(_chromosome);

            //Assert
            CollectionAssert.AreEqual(expectedmutatedChromosome, mutatedChromosome);
        }
    }
}
