﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils;

namespace EvolutionFrameworkUnitTest.EvolutionTool.Selection
{
    [TestClass]
    public class RouletteWheelSelectorTests
    {
        private Individual<int[]> _bestIndividual;
        private Individual<int[]> _worstIndividual;
        private Individual<int[]> _midIndividual;
        private List<Individual<int[]>> _individuals;
        private int _targetSize;

        [TestInitialize]
        public void TestInitialize()
        {
            _targetSize = 2;
            _bestIndividual = new Individual<int[]>(new int[] { }) { Fitness = 1 };
            _worstIndividual = new Individual<int[]>(new int[] { }) { Fitness = 0 };
            _midIndividual = new Individual<int[]>(new int[] { }) { Fitness = 0.5 };
            _individuals = new List<Individual<int[]>> { _bestIndividual, _worstIndividual, _midIndividual };
        }

        [TestMethod]
        public void WhenSelectionIsOverThenCorrectCoutOfIndividuals()
        {
            //Arrange
            IRandom random = new TestRandom(1, 2);
            RouletteWheelSelector<int[]> rouletteWheelSelector = new RouletteWheelSelector<int[]>(random);

            //Act
            List<Individual<int[]>> selectedIndividuals = rouletteWheelSelector.Select(_individuals, _targetSize).ToList();

            //Assert
            Assert.AreEqual(_targetSize, selectedIndividuals.Count);
        }

        [TestMethod]
        public void WhenSelectionIsOverThenCorrectSelection()
        {
            //Arrange
            IRandom random = new TestRandom(0.6, 2);
            RouletteWheelSelector<int[]> rouletteWheelSelector = new RouletteWheelSelector<int[]>(random);

            //Act
            List<Individual<int[]>> selectedIndividuals = rouletteWheelSelector.Select(_individuals, _targetSize).ToList();

            //Assert
            CollectionAssert.Contains(selectedIndividuals, _bestIndividual);
            CollectionAssert.DoesNotContain(selectedIndividuals, _midIndividual);
            CollectionAssert.DoesNotContain(selectedIndividuals, _worstIndividual);
        }
    }
}
