﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace EvolutionFrameworkUnitTest.EvolutionTool.Selection
{
    [TestClass]
    public class AbstractSelectionTests
    {
        private Population<int[]> _population;
        Individual<int[]> _bestIndividual;
        Individual<int[]> _worstIndividual;
        Individual<int[]> _midIndividual;

        [TestInitialize]
        public void TestInitialize()
        {
            _bestIndividual = new Individual<int[]>(new int[] {}) {Fitness = 1};
            _worstIndividual = new Individual<int[]>(new int[] { }) { Fitness = 0 };
            _midIndividual = new Individual<int[]>(new int[] { }) { Fitness = 0.5 };
            List<Individual<int[]>> individuals = new List<Individual<int[]>> { _bestIndividual, _worstIndividual };
            _population = new Population<int[]>(individuals);
            _population.AddChild(_midIndividual);
        }

        [TestMethod]
        public void WhenSelectionIsOverThenChildrenListIsEmpty()
        {
            //Arrange
            Mock<ISelector<int[]>> selector = new Mock<ISelector<int[]>>();
            selector.Setup(s => s.Select(It.IsAny<List<Individual<int[]>>>(), It.IsAny<int>())).Returns(It.IsAny<List<Individual<int[]>>>());
            Mock<AbstractSelection<int[]>> mock = new Mock<AbstractSelection<int[]>>(1, selector.Object) {CallBase = true};
            
            //Act
            mock.Object.Apply(_population);

            //Assert
            Assert.AreEqual(0, _population.Children.Count);
        }
    }
}
