﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Utils;

namespace EvolutionFrameworkUnitTest.EvolutionTool.Selection
{
    [TestClass]
    public class FromParentAndChildrenSelectionTests
    {
        private Population<int[]> _population;
        private Individual<int[]> _bestIndividual;
        private Individual<int[]> _worstIndividual;
        private Individual<int[]> _midIndividual;
        private int _targerPopulatinSize;

        [TestInitialize]
        public void TestInitialize()
        {
            _targerPopulatinSize = 2;
            _bestIndividual = new Individual<int[]>(new int[] { }) { Fitness = 1 };
            _worstIndividual = new Individual<int[]>(new int[] { }) { Fitness = 0 };
            _midIndividual = new Individual<int[]>(new int[] { }) { Fitness = 0.5 };
            List<Individual<int[]>> individuals = new List<Individual<int[]>> { _bestIndividual, _worstIndividual };
            _population = new Population<int[]>(individuals);
            _population.AddChild(_midIndividual);
        }

        [TestMethod]
        public void WhenApplyIsCallAndSelectFromSelectorIsCalled()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5, 1);
            Mock<ISelector<int[]>> selectorMock = new Mock<ISelector<int[]>>();
            selectorMock.Setup(s => s.Select(It.IsAny<IEnumerable<Individual<int[]>>>(), It.IsAny<int>()))
                .Returns(new[] {_bestIndividual});
            FromParentsAndChildrenSelection<int[]> fromParentsAndChildrenSelection = new FromParentsAndChildrenSelection<int[]>(1, selectorMock.Object);

            //Act
            fromParentsAndChildrenSelection.Apply(_population);

            //Assert
            selectorMock.Verify(s => s.Select(It.IsAny<IEnumerable<Individual<int[]>>>(), It.IsAny<int>()), Times.Once());
        }
    }
}

