﻿using System;
using System.ComponentModel;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace EvolutionFrameworkUnitTest
{
    [TestClass]
    public class GeneticMutationTests
    {
        private const int IndividualsCount = 10;
        private Population<int[]> _population;
        private List<Individual<int[]>> _individuals;

        [TestInitialize]
        public void TestInitialize()
        {
            _individuals = new List<Individual<int[]>>();
            for (int index = 0; index < IndividualsCount; index++)
            {
                _individuals.Add(new Individual<int[]>(new int[0]));
            }

            _population = new Population<int[]>(_individuals);
        }

        [TestMethod]
        public void WhenApplyIsCallAndMutationProbabilityIsOneThenMuatateIsCalledCorrectTimes()
        {
            //Arrange
            const int individualsCount = 10;
            TestRandom random = new TestRandom(0.5, 1);
            Mock<IMutationLogic<int[]>> mutationLogic = new Mock<IMutationLogic<int[]>>();
            mutationLogic.Setup(m => m.Mutate(It.IsAny<int[]>())).Returns(It.IsAny<int[]>());
            GeneticMutation<int[]> geneticMutation = new GeneticMutation<int[]>(mutationLogic.Object, random, 1);

            //Act
            geneticMutation.Apply(_population);

            //Assert
            mutationLogic.Verify( m => m.Mutate(It.IsAny<int[]>()), Times.Exactly(individualsCount));
        }

        [TestMethod]
        public void WhenMutateProbabilityZeroThenNoMutation()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5, 1);
            GeneticMutation<int[]> geneticMutation = new GeneticMutation<int[]>(null, random, 0);

            //Act
            geneticMutation.Apply(_population);

            //Assert
            Assert.AreEqual(0, _population.Children.Count);
        }
    }
}
