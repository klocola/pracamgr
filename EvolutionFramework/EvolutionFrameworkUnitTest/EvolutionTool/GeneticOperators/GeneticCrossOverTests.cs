﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using EvolutionFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace EvolutionFrameworkUnitTest.EvolutionTool.CrossOver
{
    [TestClass]
    public class GeneticCrossOverTests
    {
        private const int IndividualsCount = 10;
        private Population<int[]> _population;
        private List<Individual<int[]>> _individuals;

        [TestInitialize]
        public void TestInitialize()
        {
            _individuals = new List<Individual<int[]>>();
            for (int index = 0; index < IndividualsCount; index++)
            {
                _individuals.Add(new Individual<int[]>(new int[0]));
            }

            _population = new Population<int[]>(_individuals);
        }

        [TestMethod]
        public void WhenApplyIsCallAndCrossOverProbabilityIsOneThenCrossOverLogicIsCalledCorrectTimes()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5, 1);
            Mock<ICrossOverLogic<int[]>> crossOverLogic = new Mock<ICrossOverLogic<int[]>>();
            crossOverLogic.SetupGet(c => c.ParentCount).Returns(2);
            crossOverLogic.Setup(c => c.CrossOver(It.IsAny<List<int[]>>())).Returns(new List<int[]> {new int[0]});
            GeneticCrossOver<int[]> geneticCrossOver = new GeneticCrossOver<int[]>(crossOverLogic.Object, random, 1);

            //Act
            geneticCrossOver.Apply(_population);

            //Assert
            crossOverLogic.Verify(c => c.CrossOver(It.IsAny<List<int[]>>()), Times.Exactly(IndividualsCount / 2));
        }

        [TestMethod]
        public void WhenMutateProbabilityZeroThenNoMutation()
        {
            //Arrange
            TestRandom random = new TestRandom(0.5, 1);
            Mock<ICrossOverLogic<int[]>> crossOverLogic = new Mock<ICrossOverLogic<int[]>>();
            crossOverLogic.SetupGet(c => c.ParentCount).Returns(2);
            crossOverLogic.Setup(c => c.CrossOver(It.IsAny<List<int[]>>())).Returns(new List<int[]> { new int[0] });
            GeneticCrossOver<int[]> geneticCrossOver = new GeneticCrossOver<int[]>(crossOverLogic.Object, random, 0);

            //Act
            geneticCrossOver.Apply(_population);

            //Assert
            Assert.AreEqual(0, _population.Children.Count);
        }
    }
}
