﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;
using Utils;

namespace EvolutionFrameworkUnitTest
{
    public class TestRandom : IRandom
    {
        public TestRandom(double[] nextDouble, int[] next)
        {
            _nextDouble = nextDouble;
            _next = next;
        }

        public TestRandom(double nextDouble, int next)
        {
            _nextDouble = new[] { nextDouble };
            _next = new[] { next };
        }

        public TestRandom(int[] next)
        {
            _next = next;
        }

        public TestRandom(double[] nextDouble)
        {
            _nextDouble = nextDouble;
        }

        public TestRandom(double nextDouble)
        {
            _nextDouble = new[] { nextDouble };
        }

        public TestRandom(int next)
        {
            _next = new[] { next };
        }

        public TestRandom()
        {
        }

        private int _nextIndex;
        private readonly int[] _next;
        public int Next(int min, int max)
        {
            if (_next != null && _next.Length > 0)
            {
                int result = _next[_nextIndex];
                _nextIndex = (_nextIndex + 1) % _next.Length;
                return result;
            }

            return min + 1 > max ? max : min + 1;
        }

        private int _nextDoubleIndex;
        private readonly double[] _nextDouble = new[] { 0.3, 0.8 };
        public double NextDouble()
        {
            double result = _nextDouble[_nextDoubleIndex];
            _nextDoubleIndex = (_nextDoubleIndex + 1) % _nextDouble.Length;
            return result;
        }
    }
}
