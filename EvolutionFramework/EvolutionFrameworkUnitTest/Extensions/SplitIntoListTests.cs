﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Extensions;

namespace EvolutionFrameworkUnitTest.Extensions
{
    [TestClass]
    public class SplitIntoListTests
    {
        [TestMethod]
        public void NotEnoughElementsTooSplitInSmaeSizeGroup()
        {
            //Arrange
            List<int> listToSplit = new List<int> { 1, 2, 3, 4, 5, 6, 7 };
            const int countOfElementsInSubList = 2;
            int countOfSplitedList = listToSplit.Count/countOfElementsInSubList;

            //Act
            List<IEnumerable<int>> result = listToSplit.SplitIntoList(countOfElementsInSubList);

            //Assert
            Assert.AreEqual(countOfSplitedList, result.Count);
        }
    }
}
