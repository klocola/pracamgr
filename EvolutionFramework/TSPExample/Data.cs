﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSPExample
{
    public class TSPData
    {
        public static double[][] Cities = new []
                                       {
                                           new double[] { 0  , 23 , 45 , 56 , 678, 89 , 453, 234, 123 },
                                           new double[] { 1  , 0  , 45 , 45 , 45 , 345, 324, 23 , 234 },
                                           new double[] { 56 , 456, 0  , 95 , 104, 312, 345, 123, 345 },
                                           new double[] { 87 , 64 , 345, 0  , 345, 76 , 45 , 234, 524 },
                                           new double[] { 34 , 73 , 324, 142, 0  , 24 , 313, 243, 345 },
                                           new double[] { 101, 34 , 564, 234, 234, 0  , 563, 345, 23  },
                                           new double[] { 342, 235, 23 , 45 , 345, 34 , 0  , 563, 234 },
                                           new double[] { 235, 76 , 462, 12 , 32 , 463, 23 , 0  , 365 },
                                           new double[] { 463, 85 , 601, 99 , 87 , 456, 356, 24 , 0   }
                                       }; 
    }
}
