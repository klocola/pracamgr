﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;
using Utils;

namespace TSPExample
{
    public class PermutationConfiguration : AbstractPermutationConfiguration<int>
    {
        public PermutationConfiguration(int[] elementsInPermutation)
            :base(elementsInPermutation)
        {
        }

        public override IFitness<int[]> Fitness
        {
            get { return new Fitness(); }
        }
    }
}
