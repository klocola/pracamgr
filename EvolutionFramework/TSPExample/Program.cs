﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;

namespace TSPExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine<int[]> engine = new Engine<int[]>(new PermutationConfiguration(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 }));
            engine.RunEvolution();
            Console.ReadLine();
        }
    }
}
