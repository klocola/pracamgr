﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolutionFramework;
namespace TSPExample
{
    public class Fitness : AbstractFitness<int[]>
    {
        public override double CalculateFitness(Population<int[]> population, Individual<int[]> individual)
        {
            double sum = 0;
            for (int i = 1; i < individual.Chromosome.Length; i++ )
            {
                sum += TSPData.Cities[individual.Chromosome[i]][individual.Chromosome[i - 1]];
            }

            return 100/sum;
        }
    }
}
