﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Utils.Extensions
{
    public static class Extensions
    {
        public static TEntity OfMax<TEntity, TValue>(this IEnumerable<TEntity> enumerable, Func<TEntity, TValue> selector) where TValue : IComparable<TValue>
        {
            TEntity max = default(TEntity);
            bool isFirst = true;

            foreach (TEntity entity in enumerable)
            {
                if (isFirst || selector(max).CompareTo(selector(entity)) < 0)
                {
                    max = entity;
                    isFirst = false;
                }
            }

            return max;
        }

        public static IEnumerable<T> Clone<T>(this IEnumerable<T> listToClone) where T : ICloneable
        {
            foreach (T item in listToClone)
            {
                yield return (T)item.Clone();
            }
        }

        public static List<T> Clone<T>(this List<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

        public static List<IEnumerable<T>> SplitIntoList<T>(this List<T> list, int size)
        {
            List<IEnumerable<T>> result = new List<IEnumerable<T>>();
            for (int index = 0; index < list.Count; index += size)
            {
                if (index + size <= list.Count)
                {
                    result.Add(list.GetRange(index, size));
                }
            }

            return result;
        }

        public static bool EqualsWeakPrecision(this double val1, double val2)
        {
            const double epsilon = 1E-9;
            return Math.Abs(val1 - val2) < epsilon;
        }

        public static int IndexOf<T>(this IEnumerable<T> list, Predicate<T> condition)
        {
            int index = -1;
            return list.Any(item => { index++; return condition(item); }) ? index : -1;
        }
    }
}
